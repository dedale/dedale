package eu.mapgenerator;

import java.io.IOException;

import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.view.Viewer;

public class Barabasi {

	public static void main(String[] args) {
		Graph graph = new SingleGraph("Barabàsi-Albert");
		System.setProperty("org.graphstream.ui", "javafx");
		//// can be necessary if a bug related to quantum.QuantumToolkit.resumeTimer appears.   
		// java program starts before the JavaFX engine. 
		//Use the following system property in order to delay the start of the viewer:
		System.setProperty("org.graphstream.debug","true");
		
		Viewer v=new FxViewer(graph, FxViewer.ThreadingModel.GRAPH_IN_GUI_THREAD);//GRAPH_IN_GUI_THREAD)
		v.enableAutoLayout();
		v.setCloseFramePolicy(FxViewer.CloseFramePolicy.CLOSE_VIEWER);
		v.addDefaultView(true);
		graph.display();
		
		// Between 1 and 3 new links per node added.
		Generator gen = new BarabasiAlbertGenerator(1);
		// Generate 100 nodes:
		gen.addSink(graph); 
		gen.begin();

		for(int i=0; i<100; i++) {
			gen.nextEvents();
		}

		gen.end();
		//graph.display();
		
		try {
			graph.write("exportedFile.dgs");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
