package eu.observables;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * All the elements that are the property of an agent but will be made observable for the team's COS GUI + log
 * @author hc
 *
 */
public class AgentObservableElement {

	private String agentName;
	private int currentGoldValue;
	private int currentDiamondValue;
	private int nbMsgSent;
	private int nbMsgSentAndDelivered;
	private int nbMsgReceived;

	private PropertyChangeSupport support;

	/**
	 * Initialise the observable properties of an agent
	 * @param agentName localName of the agent
	 */
	public AgentObservableElement(String agentName) {
		this.agentName=agentName;
		currentDiamondValue=0;
		currentGoldValue=0;
		nbMsgReceived=0;
		nbMsgSentAndDelivered=0;
		nbMsgSent=0;

		support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public void setCurrentGoldValue(int goldValue) {

		support.firePropertyChange(this.agentName+";"+"gold", this.currentGoldValue, goldValue);
		this.currentGoldValue = goldValue;
	}

	public void setCurrentDiamondValue(int diamondValue) {

		support.firePropertyChange(this.agentName+";"+"diamond", this.currentDiamondValue, diamondValue);
		this.currentDiamondValue = diamondValue;
	}

	public void setCurrentNbMsgSent(int nbMsgSent) {

		support.firePropertyChange(this.agentName+";"+"sent", this.nbMsgSent, nbMsgSent);
		this.nbMsgSent = nbMsgSent;
	}

	public void setCurrentNbMsgReceived(int nbMsgReceived) {

		support.firePropertyChange(this.agentName+";"+"received", this.nbMsgReceived, nbMsgReceived);
		this.nbMsgReceived = nbMsgReceived;
	}

	public void setCurrentNbMsgSentAndDelivered(int nbMsgSentAndDelivered) {

		support.firePropertyChange(this.agentName+";"+"sentAndDelivered", this.nbMsgSentAndDelivered, nbMsgSentAndDelivered);
		this.nbMsgSentAndDelivered = nbMsgSentAndDelivered;
	}

	public String toString() {
		return this.agentName+" ;currentDiamondInBP: "+this.currentDiamondValue+";currentGoldInBP: "+this.currentGoldValue+"; nbSent: "+this.nbMsgSent+"; nbSent&Delivered: "+this.nbMsgSentAndDelivered+"; nbReceived: "+this.nbMsgReceived+"\n"; 
	}

	public String getAgentName() {
		return agentName;
	}

	public int getCurrentGoldValue() {
		return currentGoldValue;
	}

	public int getCurrentDiamondValue() {
		return currentDiamondValue;
	}

	public int getNbMsgSent() {
		return nbMsgSent;
	}

	public int getNbMsgSentAndDelivered() {
		return nbMsgSentAndDelivered;
	}

	public int getNbMsgReceived() {
		return nbMsgReceived;
	}
}
