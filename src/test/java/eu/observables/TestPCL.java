package eu.observables;

public class TestPCL {
	
 public static void main( String[]args) {
	//code within each agent
	 
	 AgentObservableElement a1= new AgentObservableElement("Agent1");
	 AgentObservableElement a2= new AgentObservableElement("Agent2");

 //code within the GUI
	 ObserverOfAgents ooas= new ObserverOfAgents();
	 ooas.addAgent2Track("Agent1");
	 ooas.addAgent2Track("Agent2");
	
	 //somewhere each agent will do:
	 a1.addPropertyChangeListener(ooas);
	 a2.addPropertyChangeListener(ooas);
	 
	 //somewhere 
	 
	 //then each time the agents update its state
	 System.out.println(a1.toString());
	 System.out.println(a2.toString());
	 System.out.println(ooas.toString());
	 
	 a1.setCurrentGoldValue(13);
	 
	 System.out.println("Ajout de 13 or dans a1");
	 System.out.println(a1.toString());
	 System.out.println(a2.toString());
	 System.out.println(ooas.toString());
	 
	 
	 a2.setCurrentNbMsgSentAndDelivered(3);
	 a2.setCurrentDiamondValue(1);
	 
	 System.out.println("Ajoute de 3 sent&R dans a2");
	 System.out.println("Ajoute de 1 diam dans a2");
	 
	 System.out.println(a1.toString());
	 System.out.println(a2.toString());
	 System.out.println(ooas.toString());
	 
	 
 }
 
 
}
