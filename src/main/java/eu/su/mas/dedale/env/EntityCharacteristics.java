package eu.su.mas.dedale.env;

import java.io.Serializable;
import java.util.Random;
import java.util.HashSet;
import java.util.Set;

import dataStructures.tuple.Couple;
import debug.Debug;
import eu.su.mas.dedale.env.mapElements.LockElement.LockType;
import eu.su.mas.dedale.princ.ConfigurationFile;


/**
 * This class contain, for each agent, all its (default) characteristics
 * @author hc
 *
 */
public class EntityCharacteristics implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7070846449444089267L;

	private final String agentName;
	private Observation myTreasureType;
	private final EntityType myEntityType;
	private final Integer diamondCapacity;
	private final Integer goldCapacity;
	private final Integer communicationReach;
	private final Location initialLocation;
	private final Integer detectionRadius;
	private Set<Couple<LockType,Integer>> expertise;

	//	TODO add all agent characteristics here
	public EntityCharacteristics(EntityType e){
		this.agentName=null;
		this.expertise=new HashSet<Couple<LockType,Integer>>();
		this.initialLocation=null;
		this.myEntityType=e;
		this.diamondCapacity=0;
		this.goldCapacity=0;
		this.communicationReach=ConfigurationFile.DEFAULT_COMMUNICATION_REACH;
		Random r;
		switch (e){
		case AGENT_COLLECTOR:
			r= new Random();
			if (r.nextInt(2)==1){
				this.myTreasureType=Observation.DIAMOND;
			}else{
				this.myTreasureType=Observation.GOLD;
			}
			this.detectionRadius=0;
			break;
		case WUMPUS:// case WUMPUS_MOVER:
			r= new Random();
			if (r.nextInt(2)==1){
				this.myTreasureType=Observation.DIAMOND;
			}else{
				this.myTreasureType=Observation.GOLD;
			}
			this.detectionRadius=ConfigurationFile.DEFAULT_DETECTION_RADIUS;
			break;
		case AGENT_EXPLORER:
			this.myTreasureType=Observation.NO_TREASURE;
			this.detectionRadius=0;
			break;
		case AGENT_TANKER:
			this.myTreasureType=Observation.ANY_TREASURE;
			this.detectionRadius=0;
			break;	
		default:
			this.myTreasureType=null;
			this.detectionRadius=0;
		}		
	}

	/**
	 * @param agentName localName
	 * @param entityType definesTheCaracteristics of the agent
	 * @param diamondCapa Quantity of diamond it can carry
	 * @param goldCapa Quantity of gold it can carry
	 * @param comReach Maximal distance from which the agent can be reached
	 * @param initialLocation null if random
	 * @param detectionRadius maximal distance from which the presence of the agent can be detected (stench in the env)
	 * @param expertise null is no lock
	 */
	public EntityCharacteristics(String agentName,EntityType entityType,int diamondCapa,int goldCapa, int comReach,Location initialLocation,int detectionRadius,Set<Couple<LockType,Integer>> expertise){
		this.agentName=agentName;
		this.expertise=expertise;
		this.diamondCapacity=diamondCapa;
		this.goldCapacity=goldCapa;
		this.communicationReach=comReach;
		this.myEntityType=entityType;
		this.initialLocation=initialLocation;
		this.detectionRadius=detectionRadius;
		//mytreasure type should be determined from the combination of EntityType and diamond/gold capacities
		switch (entityType) {
		case AGENT_EXPLORER:
			this.myTreasureType=Observation.NO_TREASURE;
			break;
		case AGENT_TANKER: case AGENT_COLLECTOR:case WUMPUS : case HUMAN: //case WUMPUS_MOVER
			if (diamondCapa>0){
				if (goldCapa>0) {
					this.myTreasureType=Observation.ANY_TREASURE;
				}else {
					this.myTreasureType=Observation.DIAMOND;
				}
			}else{
				if (goldCapa>0) {
				this.myTreasureType=Observation.GOLD;
				}else {
					this.myTreasureType=Observation.NO_TREASURE;
				}
			}
			break;
		default:
			this.myTreasureType=null;
			Debug.error(this.getClass().getName()+" - This Agent type does not currently exist"+entityType);
			break;
		}

	}

	/**
	 * 
	 * @return null if no initial location enforced , locationId otherwise
	 */
	public Location getInitialLocation() {	
		return this.initialLocation;
	}

	public Integer getGoldCapacity(){
		return this.goldCapacity;
	}

	public Integer getDiamondCapacity(){
		return this.diamondCapacity;
	}

	public Integer getDetectionRadius(){
		return this.detectionRadius;
	}
	public Observation getMyTreasureType(){
		return this.myTreasureType;
	}
	
	public void setMyTreasureType(Observation o) {
		this.myTreasureType=o;
	}

	public Integer getBackPackCapacity(Observation o) {
		switch (o) {
		case DIAMOND:
			return this.getDiamondCapacity();
		case GOLD:
			return this.getGoldCapacity();
		default:
			return 0;
		}
	}

	public Integer getCommunicationReach() {
		return communicationReach;
	}


	public EntityType getMyEntityType(){
		return myEntityType;
	}

	
	public Set<Couple<LockType,Integer>> getExpertise(){
		return this.expertise;
	}

	public String getAgentName() {
		return this.agentName;
	}

	@Override
	public String toString(){
		return "AgentName: "+agentName+"; Entitytype: "+myEntityType+ "; TreasureType : "+myTreasureType+"; diamondCapa: "+this.diamondCapacity+"; goldCapa: "+goldCapacity+"; comReach: "+communicationReach+"; Expertise: "+this.expertise.toString();
	}
	//	/**
	//	 * 
	//	 * @param o The carac (Gold)
	//	 * @param i The quantity it can wistand (50)
	//	 */
	//	public void addCaracteristic(Observation o,Integer i){
	//		this.caracs.put(o, i);
	//	}
	//	public Integer getDefaultBackPackCapacity(Observation o){
	//		return this.caracs.get(o);
	//	}




	//	public String toString(){
	//		String s="";
	//		for (Observation o:caracs.keySet()){
	//			s+=o.toString()+"\n";
	//		}
	//		return s;
	//	}

}
