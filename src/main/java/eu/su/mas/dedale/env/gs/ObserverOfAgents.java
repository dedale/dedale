package eu.su.mas.dedale.env.gs;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;

import eu.su.mas.dedale.mas.agent.knowledge.AgentObservableElement;


/**
 * Class used by the GUI to keep track of all the agents update
 * @author hc
 *
 */
public class ObserverOfAgents implements PropertyChangeListener{

	private Map<String, AgentObservableElement> aoel;

//	private PropertyChangeSupport support;
//	
	public ObserverOfAgents() {
		this.aoel=new HashMap<String, AgentObservableElement>();	
	//	this.support = new PropertyChangeSupport(this);
	}
//	
//	public void addPropertyChangeListener(PropertyChangeListener pcl) {
//		support.addPropertyChangeListener(pcl);
//	}
//	
//	public void removePropertyChangeListener(PropertyChangeListener pcl) {
//		support.removePropertyChangeListener(pcl);
//	}
	
	public void addAgent2Track(String agentName) {
		this.aoel.put(agentName,new AgentObservableElement(agentName));
	}
	
	public void removeAgent2Track(String agentName) {
		this.aoel.remove(agentName);
	}
	
	/**
	 * 
	 * @param agentName name of the agent we are looking to observe
	 * @return the element or null if not existing
	 */
	public AgentObservableElement getDataOn(String agentName) {
		return this.aoel.get(agentName);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String [] tab=evt.getPropertyName().split(";");
		String agentName=tab[0]; 
		String propertyName=tab[1]; 
		AgentObservableElement aoe=this.aoel.get(agentName);
		switch (propertyName) {
		case "gold":
			aoe.setCurrentGoldValue((int) evt.getNewValue());
			break;
		case "diamond":
			aoe.setCurrentDiamondValue((int) evt.getNewValue());
			break;
		case "sent":
			aoe.setCurrentNbMsgSent((int) evt.getNewValue());
			break;
		case "received":
			aoe.setCurrentNbMsgReceived((int) evt.getNewValue());
			break;
		case "sentAndDelivered":
			aoe.setCurrentNbMsgSentAndDelivered((int) evt.getNewValue());
			break;
		default:
			System.err.println(this.getClass().getName()+" - Property unknowned -"+propertyName);
			break;
		}
	}

	public String toString() {
		return aoel.toString();
	}




}
