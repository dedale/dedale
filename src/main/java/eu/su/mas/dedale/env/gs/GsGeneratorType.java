package eu.su.mas.dedale.env.gs;

import org.graphstream.algorithm.generator.BarabasiAlbertGenerator;
import org.graphstream.algorithm.generator.DorogovtsevMendesGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.GridGenerator;

import eu.su.mas.dedale.env.IGeneratorType;


public enum GsGeneratorType implements IGeneratorType {

	GS_GRID {
		public Generator init(Integer ... param) {
			return new GridGenerator();
		}

	},
	GS_DOROGOVTSEV   { 
		public Generator init(Integer ... param1) { 
			return new DorogovtsevMendesGenerator();
		} 
	},
	GS_BARABASI_ALBERT   { 
		/**
		 *@param1 number of edges per new node chosen randomly between 1 and n
		 */
		public Generator init(Integer ...param1) { 
			return new BarabasiAlbertGenerator(param1[1]);//param[0] is the size of the env 
		} 
	};


	public abstract Generator init(Integer ...params);

}
