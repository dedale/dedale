package eu.su.mas.dedale.env.mapElements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.mapElements.LockElement.LockType;


public class LockElement {

	/**
	 * Types of criteria used for lock picking in the environment: lockpicking,strength
	 * Not known by the user.
	 * @author hc
	 *
	 */
	public enum LockType implements Serializable {

		LOCKPICKING("LockPicking"),
		STRENGTH("Strength");

		private String name;

		private LockType(String name) {
			this.name=name;
			//this.value=value;
		}

		@Override
		public String toString(){
			return this.name;
		}


		public String getName(){
			return this.name;
		}
	}

	private boolean lockIsOpen;
	private Set<Couple<LockType,Integer>> lockcriteria;

	/**
	 * Create a lock, closed by default
	 */
	public LockElement(){
		lockIsOpen=false;
		this.lockcriteria=new HashSet<Couple<LockType,Integer>>();
	}

	/**
	 * Create a lock with its opening criteria list. The lock starts as closed.
	 * @param openningCriteria conditions to set for a given lock
	 */
	public LockElement(Set<Couple<LockType,Integer>> openningCriteria){
		this.lockIsOpen=false;
		this.lockcriteria=openningCriteria;
	}

	/**
	 * Adding an opening criteria to an existing lock
	 * @param c criteria to add
	 */
	public void addCriteria(Couple<LockType,Integer> c){
		if (this.lockcriteria!=null){
			this.lockcriteria.add(c);
		}
	}
	
	/**
	 * 
	 * @return true is the object is already open
	 */
	public boolean isOpen(){
		return this.lockIsOpen;
	}

	/**
	 * 
	 * @param expertise the level of knowledge used to try to open the lock
	 * @return true is the lock is now open
	 */
	public boolean tryToOpen(Map<LockType,Integer> expertise){
		boolean metcriteria=true;
		
		//for all elements in the lockcriteria, check if the expertise is above or equal
		if (!this.lockIsOpen){
			for (Couple<LockType,Integer> c:lockcriteria){
				metcriteria= metcriteria && (c.getRight()==0 || (c.getRight()<=expertise.get(c.getLeft())));
			}
			this.lockIsOpen=metcriteria;
		}
		return this.lockIsOpen;
	}
	
	/**
	 * Close the lock
	 */
	public void close() {
		this.lockIsOpen=false;
	}

	/**
	 * 
	 * @param attribute the lockType to test
	 * @param expertise
	 * @return the associated value (0 if no expertise associated to this LockType)
	 */
	private Integer getExpertise(LockType attribute, Set<Couple<LockType, Integer>> expertise) {
		Assert.assertNotNull(expertise);
		Assert.assertNotNull(attribute);
		boolean found=false;
		Iterator<Couple<LockType, Integer>> iter=expertise.iterator();
		while (iter.hasNext()){
			Couple<LockType, Integer> c = iter.next();
			if (attribute.getName().equalsIgnoreCase(c.getLeft().getName())){
				return c.getRight();
			}
		}
		return 0;
	}

	/**
	 * 
	 * @return the list of criteria required to open the object
	 */
	public Set<Couple<LockType,Integer>> getOpenningCriteria(){
		return this.lockcriteria;
	}

	public String toString(){
		return "Lock status (true if open): "+this.lockIsOpen+";"+this.lockcriteria.toString();
	}


}
