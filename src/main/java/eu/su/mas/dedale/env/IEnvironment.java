package eu.su.mas.dedale.env;

import java.util.List;

import dataStructures.tuple.Couple;

/**
 * Interface that any environment for Dedale's agent should implement
 * @author hc
 *
 */
public interface IEnvironment {

	/**
	 * Create an environment.
	 * @param g Type of environment (generated or manually designed)
	 * @param topologyConfigurationFilePath FullPath to the file describing the topology
	 * @param instanceConfiguration FullPath to the file describing the elements available on the map
	 * @param diamond true if an automatically generated env should have diamond nodes on it
	 * @param gold true if an automatically generated env should have gold nodes on it
	 * @param well true if an automatically generated env should have wells on it
	 * @param topologyParameters parameters used by the choosen generator. The number of parameters is generator dependent (size,...)
	 * 
	 * All existing features are not available in randomly generated env : Treasure Locks,.. 
	 */
	public void createEnvironment(GeneratorType g,String topologyConfigurationFilePath, String instanceConfiguration,boolean diamond,boolean gold,boolean well,Integer...topologyParameters);
	
	/**
	 * Add an entity to the graph
	 *
	 * @param entityName the name of the entity to deploy
	 * @param e the {@link EntityCharacteristics} of the entity
	 * @param locationId the (unique) Id of the position to deploy the agent, null if free
	 */
	public void deployEntity(String entityName, EntityCharacteristics e,Location locationId);
	
	public void removeEntity(String entityName,EntityCharacteristics e);
	
	/**
	 * @return The name of the environment
	 */
	public String getName();
	
	/**
	 * @param entityName Name of the agent to locate
	 * @return The entity current position, null if the entity is not in the environment
	 */
	public Location getCurrentPosition(String entityName);
	
	/**
	 * @param currentPosition the position from which the agent observe
	 * @param agentName the name of the agent
	 * @return The list of observed positions, and for each one the list of observations
	 */
	public List<Couple<Location, List<Couple<Observation,String>>>> observe(Location currentPosition,String agentName);
	
	/**
	 * @param entityName the name of the entity
	 * @param ec characteristics of the entity
	 * @param targetedPosition its expected destination
	 * @return 1 if true, 0 if refused, -1 if the consequence of the move is the agent dying
	 */

	public Integer moveTo(String entityName, EntityCharacteristics ec, Location targetedPosition);
	
	
		
	/**
	 * 
	 * @param entityName the name of the entity
	 * @param location the position to pick the resource
	 * @param e ElementType to pick
	 * @param maxQuantity maximum number of e that the agent is able to pick
	 * @return the amount of resources that the agent was able to collect
	 */
	//@param currentPosition its current position
	
	public int pick(String entityName,Location location, ElementType e,Integer maxQuantity);
	
	
	/**
	 * 
	 * @param e Type of resource to unlock in the environment (Currently only gold or diamond )
	 * @param entityName the name of the entity
	 * @param location the position to pick the resource
	 * @return True is the lock is opened, false otherwise
	 */
	public boolean openLock(String entityName, Location location, ElementType e);
		
	
	/**
	 * @param location the position to close the safe(s)
	 * @return True if there was at least one safe on the current position (they are now closed), false otherwise
	 */
	public boolean closeLock(Location location);
		
	
//	/**
//	 * 
//	 * @param agentName the name of the agent
//	 * @return A list containing, for each type of resources, the remaining place in the backpack of the agent.
//	 */
//	public List<Observation> getBackPackFreeSpace(String agentName);
//		
	
	/**
	 * This methods can only be called by specific types of agents. For the others it will do nothing.
	 * @param location id of the node where to drop the element
	 * @param e the element the entity intends to drop
	 * @param quantity the amount of value to drop on the current position
	 * @return true if the drop off was realised
	 * 
	 */
	public boolean dropOff(Location location, ElementType e,Integer quantity);
	
		
		
	/**
	 * @param agentName The name of the agent that is shooting
	 * @param locationId the id of the room it aims at
	 * @return true if done, false otherwise
	 */
	public boolean throwGrenade(String agentName, Location locationId);
	
	/**
	 * 
	 * @param senderName the senderAgent
	 * @param receiverName The agent we are trying to contact
	 * @param communicationReach the sender agent's com capability
	 * @return true if the receiver is within reach
	 */
	public boolean isReachable(String senderName, String receiverName, int communicationReach);

		
}
