package eu.su.mas.dedale.env;

import java.io.Serializable;

import eu.su.mas.dedale.env.mapElements.LockElement;

/**
 * This class contain, for any element, its characteristics
 */
public class ElementCharacteristics implements Serializable {

	private static final long serialVersionUID = -7733308028177183422L;

	private final ElementType myElementType;
	private final Location initialLocation;
	private final Integer detectionRadius;
	private final LockElement myLock;
	

	public ElementCharacteristics(ElementType e) {
		//ITS 
		this.myElementType = e;
		this.initialLocation = null;
		this.detectionRadius = e.getRadius();
		this.myLock = new LockElement();
 }
	public ElementCharacteristics(ElementType myElementType, Location initialLocation, Integer detectionRadius,
			LockElement myLock) {
		super();
		this.myElementType = myElementType;
		this.initialLocation = initialLocation;
		this.detectionRadius = detectionRadius;
		this.myLock = myLock;
	}

	public ElementType getMyElementType() {
		return myElementType;
	}
	public Location getInitialLocation() {
		return initialLocation;
	}
	public Integer getDetectionRadius() {
		return detectionRadius;
	}
	public LockElement getMyLock() {
		return myLock;
	}
	
	
}
