package eu.su.mas.dedale.env.mapElements;

import java.io.Serializable;

import debug.Debug;


/**
 * This class contain, for each element on the map, its characteristics<p>
 *  - Treasures (gold and diamond) : lock status 
 *  - Well : detection radius
 *   - 
 * @author hc
 *
 */
public abstract class ElementCharacteristics implements Serializable {

	private static final long serialVersionUID = -605442449037256321L;

	private Integer radius;
	private String name;
	
	
	public ElementCharacteristics(String name, int radius){	
		this.radius=radius;
		this.name=name;
	}

	/***
	 * 
	 * @return the influence radius of the given component (0,1,2,..)
	 */
	public int getRadius(){
		return this.radius;
	}

	/**
	 * 
	 * @return the name of the component
	 */
	public String getName(){
		return this.name;
	}
	
	public String toString(){
		return "Name: "+this.name+ "radius: "+this.radius;
	}

	
	
}
