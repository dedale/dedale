package eu.su.mas.dedale.env;

/**
 * Types of topology generators that can be selected by the user
 * <pre>
 * MANUAL : The user provides the topology
 * GS_GRID : The generated 2D discrete topology will be a grid
 * GS_DOROGOVTSEV : arXiv:cond-mat/0106144v2
 * GS_BARABASI_ALBERT : doi:10.1126/science.286.5439.510
 * </pre>
 * @author hc
 *
 */
public enum GeneratorType {
	MANUAL, 
	//GraphStream
	GS_GRID,GS_DOROGOVTSEV,GS_BARABASI_ALBERT,
	//JME
	
}
