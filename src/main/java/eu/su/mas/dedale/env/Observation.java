package eu.su.mas.dedale.env;

import java.io.Serializable;

/**
 * Known by the user <p>
 * The type of data the agents can observe from the environment<p>
 * 
 * @author hc
 *
 */
public enum Observation implements Serializable{

	/** treasure type of the entity */
	
	GOLD("Gold"),
	/** treasure type of the entity */
	DIAMOND("Diamond"),
	
	/** treasure type of the entity */
	ANY_TREASURE("Any"),
	/** treasure type of the entity */
	NO_TREASURE("None"),
	
	/**
	 * A golem releases its stench in its vicinity
	 */
	STENCH("Stench"),
	
	/**
	 * A well generates wind around itself
	 */
	
	WIND("WIND"),
	
	/**
	 * The status of a treasure lock
	 */
	LOCKSTATUS("LockIsOpen"),
	/**
	 * The strength of an agent, part of its expertise
	 */
	STRENGH("Strength"),
	/**
	 * The lockpicking expertise of an agent, part of its expertise
	 */
	LOCKPICKING("LockPicking"),
	
	/**
	 * The name of an agent observed in the vicinity
	 */
	AGENTNAME("AgentName");
	
	private String name;
	//private Integer value;
	
	private Observation(String name) {
		this.name=name;
		//this.value=value;
	}
	
	
	
	@Override
	public String toString(){
		return this.name;
	}
	
	
	public String getName(){
		return this.name;
	}
}
