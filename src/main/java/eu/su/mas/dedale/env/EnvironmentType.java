package eu.su.mas.dedale.env;

/**
 * Supported types of environment :
 * <pre>
 * Gs : GraphStream
 * Jme : Java Monkey Engine
 * Unity : to come
 * Morse : to come
 * </pre>
 * @author hc
 *
 */
public enum EnvironmentType {

	JME,GS,UNITY,MORSE;
	
}
