package eu.su.mas.dedale.env.gs;

import eu.su.mas.dedale.env.Location;

/**
 * Location component for the graphStream environment
 * @author hc
 *
 */
public class GsLocation implements Location {
	private String nodeId;

	private static final long serialVersionUID = 192266977093273239L;

	/**
	 * Create a new location 
	 * @param nodeId the string representation of this location identifier (must be unique) 
	 */
	public GsLocation(String nodeId) {
		this.nodeId=nodeId;
	}
	
	@Override
	public String toString() {
		return  nodeId;
	}
	
	/** 
	 *@return the string representation of the identifier of this location 
	 */
	public String getLocationId() {
		return nodeId;
	}
	
	@Override
	public boolean equals(Location l) {
		return this.nodeId.equalsIgnoreCase(l.toString());
	}
	
	
}

