package eu.su.mas.dedale.env;

import org.graphstream.algorithm.generator.Generator;

public interface IGeneratorType {
	
	abstract Generator init(Integer ...params);

}
