package eu.su.mas.dedale.env.gs;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

import java.util.stream.Collectors;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.ui.fx_viewer.FxViewPanel;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.javafx.FxGraphRenderer;
import org.graphstream.ui.view.Viewer;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.Validate;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.algorithm.Dijkstra.Element;
import org.graphstream.algorithm.generator.DorogovtsevMendesGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.algorithm.generator.GridGenerator;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.graph.implementations.SingleGraph;

import org.graphstream.stream.file.FileSource;
import org.graphstream.stream.file.FileSourceFactory;

import dataStructures.tuple.Couple;
import debug.Debug;

import eu.su.mas.dedale.env.ElementType;
import eu.su.mas.dedale.env.EntityCharacteristics;
import eu.su.mas.dedale.env.EntityType;
import eu.su.mas.dedale.env.GeneratorType;
import eu.su.mas.dedale.env.IEnvironment;

import eu.su.mas.dedale.env.InGameConfigurationFile;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.mapElements.LockElement;
import eu.su.mas.dedale.env.mapElements.LockElement.LockType;
import eu.su.mas.dedale.gui.JavaFxmlGui;
import eu.su.mas.dedale.gui.MyController;
import eu.su.mas.dedale.mas.agent.knowledge.AgentObservableElement;
import eu.su.mas.dedale.tools.ElementCharacteristicsFromJSON;
import javafx.fxml.FXMLLoader;

import javafx.application.*;




/**
 * This class is an implementation of the IEnvironment using the graphStream library
 * 
 * It currently offers : 
 *  - several environments generators and any predefined ones that respect the DGS format  
 *  - 3 types of agents (tanker, explorer and collector), 
 *  - 2 type of wumpus (which generates stench),
 *  - 1 type of keyboardControlled agent (Human), that must be unique on a given instance.
 *  - Well (that generate wind and kill the agents that come in them),
 *  - 2 types of treasures (gold and diamonds) with safes with 2 opening conditions (lockpicking expertise and strength)
 *  
 * @author hc
 *
 */
public class GsEnvironment implements IEnvironment {


	/***********************************
	 * 
	 *  		ATTRIBUTES
	 * 
	 ************************************/
	//GUI parameters

	private final static String defaultNodeStyle= "node {"+"fill-color: white;"+" size-mode:fit;text-alignment:center; text-size:12;text-color:black;text-background-mode:rounded-box;text-background-color:white;}";
	private final static String nodeStyle_wumpus= "node.wumpus {"+"text-alignment:under;size:10;text-background-color:white;fill-color: red;"+"}";
	private final static String nodeStyle_agentCollect= "node.agentcollect {"+"text-alignment:under;size:10;text-background-color:white;fill-color:blue;"+"}";
	private final static String nodeStyle_agentExplo= "node.agentexplo {"+"text-alignment:under;size:10;text-background-color:white;fill-color: forestgreen ;"+"}";
	private final static String nodeStyle_agentTanker= "node.agenttanker {"+"text-alignment:under;size:10;text-background-color:white;fill-color: violet;"+"}";

	private final static String nodeStyle_Human= "node.human {"+"text-alignment:under;size:10;text-background-color:white;fill-color: red;"+"}";
	private final static String edgeStyle_Human= "edge.human {"+"text-alignment:under;size:2;text-background-color:white;fill-color: red;"+"}";

	private final static String nodeStyle_gold= "node.gold {"+"size-mode:normal;fill-color: yellow;shape:diamond;size:35;text-background-color:yellow;"+"}";
	private final static String nodeStyle_diamonds= "node.diamonds {"+"size-mode:normal;fill-color: green;shape:diamond;size:35;text-background-color:green;"+"}"; //!! diamond without s seems to be a reserved keyword in gs
	private final static String nodeStyle_EntryExit= "node.exit {"+"fill-color: gray;"+"}";

	private final static String nodeStyle_Well= "node.well {"+"fill-color: cyan;shape:cross;size:20;text-alignment:under;text-background-color:white;"+"}";
	private final static String nodeStyle_Wind= "node.wind {"+"fill-color: pink;text-background-color:pink;"+"}";
	private final static String nodeStyle_Stench= "node.stench {"+"fill-color: orange;text-background-color:orange;"+"}";

	private final static String nodeStyle=defaultNodeStyle+nodeStyle_wumpus+nodeStyle_agentExplo+nodeStyle_agentTanker+nodeStyle_agentCollect+nodeStyle_gold+nodeStyle_diamonds+nodeStyle_EntryExit+nodeStyle_Well+nodeStyle_Wind+nodeStyle_Stench+nodeStyle_Human+edgeStyle_Human;

	/**
	 * temporary attributes for com and treasure visualization before javaFx
	 */
	//	private static final String nodeID_treasureAgregated_team = "cat: T-Team";
	//	private static final String nodeID_treasureAgregated_silo = "cat: T-Silo";
	//	private static final String nodeID_treasureAgregated_golem = "cat: T-Golem";
	//	private static final String nodeID_treasureRemaining = "cat: T-Env";
	//	private static final String nodeID_diamondAgregated_team = "cat: D-Team";
	//	private static final String nodeID_diamondAgregated_golem = "cat: D-Golem";
	//	private static final String nodeID_diamondRemaining = "cat: D-Env";
	//	private static final String nodeID_communication = "cat: Com-nbMsgs";


	private final static String STYLE_SHEET = "graph {padding: 60px;}";

	private Graph graph;

	/**
	 * Attributes for the GUI
	 */
	private Viewer viewer;
	private MyController m;

	private boolean osm= false;

	private String environmentName;

	private Random r;

	/**
	 * This attribute is used to that the environment knows all the agents expertise.
	 * Its a temporary solution as the treasures will/should become artefact (objects and messages) in the next evolution of the platform.
	 * The key is the agentName
	 */
	private Map<String,Set<Couple<LockType,Integer>>>agentsExpertise;

	/**
	 * Temporary to be used in graphical rendering before javaFx.
	 * These information should be personal to the agents then aggregated by the GK
	 */
	//private HashMap<String, Integer> agentBackpackFreeCapacity_gold;
	//private HashMap<String, Integer> agentBackpackFreeCapacity_diamond;
	//private HashMap<String, Integer> agentCurrentGold;
	//private HashMap<String, Integer> agentCurrentDiamond;

	private AgentObservableElement aoenv;

	/**
	 * This object observe the agent observablesElements and is updated each time there is a change.
	 * At the same time, it is observed by the GUI
	 */
	private ObserverOfAgents ooa;

	/**
	 * 
	 * @param topologyConfigurationFilePath fullPath towards the topology file 
	 * @param instanceConfiguration fullPath towards the file indicating the elements on the map
	 * @param g type of generator to use 
	 * @param diamond true is we should generate diamonds
	 * @param gold true is we should generate golds
	 * @param well true is we should generate wells
	 * @param topologyParameters at least, indicate the size of the environment to generate, if necessary add the generatorType specific parameters
	 */
	public void createEnvironment(GeneratorType g,String topologyConfigurationFilePath, String instanceConfiguration,boolean diamond,boolean gold,boolean well,Integer...topologyParameters) {
		//	TODO allow the generation of elements on a loaded topology
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		r= new Random();
		this.environmentName="env";
		this.agentsExpertise=new HashMap<String,Set<Couple<LockType,Integer>>>();

		// *********************
		this.ooa = new ObserverOfAgents();
		this.aoenv=new AgentObservableElement(this.environmentName);
		//linking the env
		this.ooa.addAgent2Track(this.environmentName);
		this.aoenv.addPropertyChangeListener(this.ooa);

		//1)load topology and elements on it
		GsGeneratorType g2=null;
		switch (g) {
		case MANUAL:
			Objects.requireNonNull(topologyConfigurationFilePath, "The topology configuration should be given");
			Objects.requireNonNull(instanceConfiguration,"The instance configuration should be given (elements ont the map)");

			//1.a) Load topology
			loadGraph(topologyConfigurationFilePath);
			//1.b) Load elements
			Objects.requireNonNull(instanceConfiguration, "The file describing the list of elements on the Map is not given");
			if (instanceConfiguration.endsWith(".json")){
				loadMapElementsFromJson(instanceConfiguration);
			}else {
				loadMapElementsFromTxt(instanceConfiguration);
			}
			break;
		case GS_BARABASI_ALBERT: 
			g2=GsGeneratorType.GS_BARABASI_ALBERT;
			break;
		case GS_DOROGOVTSEV:
			g2=GsGeneratorType.GS_DOROGOVTSEV;
			break;
		case GS_GRID:
			g2=GsGeneratorType.GS_GRID;
			break;
		default:
			Debug.error("The type of generator given in parameter does not exist with GraphStream: "+ g+ "/ Change it in the Configuration file");
			break;
		}

		if (g2!=null) {
			generateGraph(g2,topologyParameters);

			//2)addComponents
			addElements(diamond,gold,well);
		}

		//*************************

		//3) define GUI parameters
		this.graph.setAttribute("ui.stylesheet", nodeStyle);

		//this.graph.setProperty("org.graphstream.ui")
		/** work before javaFx ***/
		//this.graph.display(true);
		//this.viewer=this.graph.display();
		/** end before javaFx**/
		/** with javaFx**/
		new Thread(() -> {
			Application.launch(JavaFxmlGui.class, null);
		}).start();

		JavaFxmlGui startUpTest = JavaFxmlGui.waitForStartUpTest();

		//Test 1 : pb, **  Uncaught Exception for agent GK  ***
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				// Update UI here.
				FXMLLoader loader= startUpTest.getLoad();
				//System.out.println("Loader2: "+loader);
				//oldMyController m=loader.getController();
				m =loader.getController();
				//System.out.println("controller2: "+m);

				Objects.requireNonNull(m);
				m.setGraph(getJavaFxViewer());
				//connect env
				m.connectGuiWithAgentObservable(ooa.getDataOn(environmentName));
			}
		});

	}


	/**
	 *@param entityName Should be unique
	 *@param e Type of the agent
	 *@param locationId position where to deploy the agent, null if free
	 *
	 * Add the agent entityName of type e on the position locationId
	 */
	public synchronized void deployEntity(String entityName, EntityCharacteristics e,Location locationId) {
		if (locationId==null){
			deployEntityFromVoid(entityName,e);
		}else{
			deployEntityFromConfig(entityName,e,locationId);
		}
		//temp to manage gui
		//this.agentCurrentGold.put(entityName,0);
		this.ooa.addAgent2Track(entityName);
		this.m.connectGuiWithAgentObservable(this.ooa.getDataOn(entityName));

	}

	private void deployEntityFromVoid(String entityName, EntityCharacteristics e) {
		//An agent with the same name should not be already on the map
		Objects.requireNonNull(entityName, "A name is required");
		Location currentLocation=getCurrentPosition(entityName);
		if (currentLocation!=null) {
			System.err.println("An entity with the same name is already deployed");
			System.exit(-1);
		}

		//not existing, random deployment
		Node n=org.graphstream.algorithm.Toolkit.randomNode(this.graph);

		boolean free = isOkToDeployEntityOn(n.getId());
		//TODO Guarantee non infinite loop
		while(!free){//We assume that there exist at least one free node
			n=org.graphstream.algorithm.Toolkit.randomNode(this.graph);
			free=isOkToDeployEntityOn(n.getId());
		}

		if (free){
			Debug.info("Entity " + entityName +" deployed in " + n.getId()+"\n Characteristics: "+ " type : "+e);
			addExpertise(entityName,e);
			indicateEntityPresence(n, e,entityName);
		}else{// no free node
			Debug.error("Impossible to deploy entity " + entityName + ", no free position remaining (max one agent for each node)");
		}
	}

	private void deployEntityFromConfig(String entityName, EntityCharacteristics e, Location location) {
		//An agent with the same name should not be already on the map
		Location nodeId=getCurrentPosition(entityName);
		if (nodeId!=null) {
			System.err.println("An entity with the same name is already deployed");
			System.exit(-1);
		}
		Objects.requireNonNull(e);
		Objects.requireNonNull(location,"A coordinate to deploy the entity on should be given");

		Node n = this.graph.getNode(((GsLocation)location).getLocationId());
		if (n == null)	{
			Debug.error("Impossible to deploy entity " + entityName + ", the indicated position does not exist: "+location);
		}else{
			boolean free = isOkToDeployEntityOn(n.getId());
			if (free)	{
				System.out.println("Entity " + entityName + " of type : "+e.toString()+", deployed in " + n.getId()+"\n Characteristics: "+" type : "+e.toString());
				addExpertise(entityName,e);				
				//n.setAttribute(e.getMyEntityType().getName(), entityName);
				indicateEntityPresence(n, e,entityName);
				//updateNodeRendering(n);
			}else {
				Debug.error("Impossible to deploy entity " + entityName + ", the indicated position is not free");
			}
		}
	}


	private void addExpertise(String entityName, EntityCharacteristics e) {
		this.agentsExpertise.put(entityName, e.getExpertise());
	}


	public synchronized void removeEntity(String entityName,EntityCharacteristics e) {
		Location currentLocation=this.getCurrentPosition(entityName);
		try {
			Objects.requireNonNull(currentLocation, "The entity "+entityName+ "was not found in the environment");
			Node n=this.graph.getNode(((GsLocation)currentLocation).getLocationId());
			clearEntityPresence(n,entityName, e);
		}catch(NullPointerException e1) {
		}

	}


	/**
	 * 
	 * @param n
	 * @param entityName the name of the entity to remove (currently not used as two agents cannot be on the same location)
	 * @param e
	 */
	private synchronized void clearEntityPresence(Node n, String entityName, EntityCharacteristics e) {

		//1° get the node to update
		Set<Node> n2update=findNeighbours(n,e.getDetectionRadius());

		//2° update them
		switch (e.getMyEntityType()) {
		case WUMPUS: //case WUMPUS_MOVER :
			n.removeAttribute(ElementType.STENCH.getName());
			n.removeAttribute(e.getMyEntityType().getName());
			updateNodeRendering(n);
			//			n2update.remove(n);
			//			for (Node n2:n2update){
			//				n2.removeAttribute(ElementType.STENCH.getName());
			//				updateNodeRendering(n2);
			//				//if this is not a well or a wind node, change the rendering
			//			}
			break;
		case AGENT_COLLECTOR : case AGENT_EXPLORER : case AGENT_TANKER:   
			n.removeAttribute(e.getMyEntityType().getName());
			updateNodeRendering(n);
			break;
		case HUMAN:
			n.removeAttribute(e.getMyEntityType().getName());
			updateNodeRendering(n);
			Edge edge= (Edge) n.edges().filter(x -> x.hasAttribute(ElementType.PATH.getName())).toArray()[0];
			if (edge!=null) {
				edge.removeAttribute(ElementType.PATH.getName());
				updateEdgeRendering(edge);
			}else {
				Debug.error(this.getClass().getName() + " clearEntityPresence, this should not happen, signal the bug thanks");
			}

			break;
		default:
			break;
		}
		//stench for any agent with detection radius positive should be cleared
		n2update.remove(n);
		for (Node n2:n2update){
			n2.removeAttribute(ElementType.STENCH.getName());
			updateNodeRendering(n2);
			//if this is not a well or a wind node, change the rendering
		}
	}

	/**
	 * 
	 * @param n node to update
	 * @param e element to remove
	 */
	private synchronized void clearElementPresence(Node n, ElementType e){
		n.removeAttribute(e.getName());
		updateNodeRendering(n);
	}

	public synchronized Location getCurrentPosition(String entityName) {
		Iterator<Node> nodeCollection=this.graph.iterator();
		boolean found=false;
		Node n=null;

		while (!found && nodeCollection.hasNext()){
			n= nodeCollection.next();
			found=n.hasAttribute("ui.label") && entityName.equals(n.getAttribute("ui.label").toString());
		}
		if (found){
			return new GsLocation(n.getId());
		}else{
			//Debug.warning("getCurrentPosition - The entity "+entityName +" was not found in the environment.");
			return null;
		}
	}


	public synchronized List<Couple<Location, List<Couple<Observation, String>>>> observe(Location currentPosition, String agentName) {
		List<Couple<Location, List<Couple<Observation, String>>>> l= new ArrayList<Couple<Location, List<Couple<Observation, String>>>>();
		Node n=this.graph.getNode(((GsLocation)currentPosition).getLocationId());

		if(!n.getAttribute("ui.label").toString().contains(agentName)){//There is no agent here so you can't call observe
			Debug.error("You can't observe from this position (cheater..)");
		}

		//First we capture observation ON the current location
		List<Couple<Observation, String>> la= getObservations(n,true);
		Couple<Location,List<Couple<Observation, String>>> c=new Couple<Location,List<Couple<Observation, String>>>(new GsLocation(n.getId()),la);

		l.add(c);
		//Then we capture information in the vicinity (radius = 1 only)
		Iterator<Node>iter=n.neighborNodes().iterator();// getNeighborNodeIterator();
		while(iter.hasNext()){
			Node temp=iter.next();
			la= getObservations(temp,false);
			c=new Couple<Location,List<Couple<Observation, String>>>(new GsLocation(temp.getId()),la);	
			l.add(c);
		}

		return l;
	}

	/**
	 * Synchronized method to 
	 * @param n the node to consider
	 * @param onIt true if the agent is in node n, false otherwise. If false, the distance is necessarily equal to 1.
	 * @return the associated observations (at the moment of the access)
	 */
	private synchronized List<Couple<Observation, String>> getObservations(Node n,Boolean onIt) {
		Objects.requireNonNull(n);
		Objects.requireNonNull(onIt);

		Iterator<String>	iter=n.attributeKeys().iterator();//getAttributeKeyIterator();

		List<Couple<Observation, String>> l= new ArrayList<Couple<Observation, String>>();

		//Iterator on the attributes of node n
		while(iter!=null && iter.hasNext()){
			String attrib=iter.next();
			//We detect the wind from a well 
			if (ElementType.WIND.getName().equalsIgnoreCase(attrib)){
				l.add(new Couple<Observation, String>(Observation.WIND,null));
			}

			if(onIt && ElementType.GOLD.getName().equals(attrib)){
				//Attribute a=Attribute.TREASURE;
				//a.setValue(n.getAttribute(attrib));
				l.add(new Couple<Observation, String>(Observation.GOLD,n.getAttribute(attrib).toString()));
				//l.add(new Couple<Observation, Integer>(Observation.LOCKPICKING, (Integer)n.getAttribute(attrib)));
			}

			if(onIt && ElementType.DIAMOND.getName().equals(attrib)){
				//Attribute a=Attribute.DIAMONDS;
				//a.setValue(n.getAttribute(attrib));
				l.add(new Couple<Observation, String>(Observation.DIAMOND,n.getAttribute(attrib).toString()));
			}
			if(onIt && ElementType.LOCK.getName().equals(attrib)){
				LockElement lock= (LockElement)n.getAttribute(attrib);
				Set<Couple<LockType, Integer>>lockcriteria=((LockElement)n.getAttribute(attrib)).getOpenningCriteria();
				l.add(new Couple<Observation, String>(Observation.LOCKSTATUS,lock.isOpen()?"1":"0"));
				Iterator<Couple<LockType, Integer>> iter2=lockcriteria.iterator();
				while (iter2.hasNext()){
					Couple<LockType, Integer> cpl=iter2.next();
					if(cpl.getLeft().getName().equalsIgnoreCase(Observation.LOCKPICKING.getName())){
						l.add(new Couple<Observation, String>(Observation.LOCKPICKING,cpl.getRight().toString()));
					}
					if(cpl.getLeft().getName().equalsIgnoreCase(Observation.STRENGH.getName())){
						l.add(new Couple<Observation, String>(Observation.STRENGH,cpl.getRight().toString()));
					}
				}

			}

			//We detect the stench of a wumpus
			if (ElementType.STENCH.getName().equals(attrib)){
				l.add(new Couple<Observation,String>(Observation.STENCH,null));
			}
			//we detect a wumpus
			if (!onIt && EntityType.WUMPUS.getName().equals(attrib)) {
				l.add(new Couple<Observation, String>(Observation.AGENTNAME,EntityType.WUMPUS.getName()));//(String) n.getAttribute(attrib)
			}

			//we detect an agent
			if (!onIt && (EntityType.AGENT_COLLECTOR.getName().equals(attrib) || EntityType.AGENT_EXPLORER.getName().equals(attrib) || EntityType.AGENT_TANKER.getName().equals(attrib))) {
				l.add(new Couple<Observation, String>(Observation.AGENTNAME,(String) n.getAttribute(attrib)));
			}

		}
		return l;
	}


	public synchronized Integer moveTo(String entityName, EntityCharacteristics e, Location targetedPosition) {
		Objects.requireNonNull(entityName,"Agent name should not be empty");
		Objects.requireNonNull(targetedPosition,"The targeted location should not be null");
		Objects.requireNonNull(e,"The entityCharacterstics should not be null");

		Location currentPosition = this.getCurrentPosition(entityName);	
		Objects.requireNonNull(currentPosition,"The agent current position cannot be nul");
		if (currentPosition.equals(targetedPosition)){
			return 1;
		}

		Node current=this.graph.getNode(((GsLocation)currentPosition).getLocationId());
		Node target=this.graph.getNode(((GsLocation)targetedPosition).getLocationId());

		//System.out.println("Move function");
		//System.out.println("Agent "+ agentName+ ", current node"+currentNodeId+", target: "+targetedNodeId);

		if(current.hasEdgeBetween(target)){
			//if the target is reachable, the move is legit

			//if the target is a well, kill the agent. Otherwise move
			if (target.hasAttribute(ElementType.WELL.getName())){
				//update the state of the original node
				clearEntityPresence(current,entityName,e);
				return -1;	
			}else{
				boolean targetEmpty=isPossibleToMove(((GsLocation)targetedPosition).getLocationId());

				if (targetEmpty){
					//move authorized
					clearEntityPresence(current,entityName,e);
					indicateEntityPresence(target, e, entityName);
					return 1;	
				}else{
					//move forbidden due to the presence of another entity

					if (e.getMyEntityType()==EntityType.WUMPUS) {// || e.getMyEntityType()==EntityType.WUMPUS_MOVER) {
						//specific case to deal with several wumpus stench overlapping, could be generalized but not useful for now. see issue #64
						clearEntityPresence(current,entityName,e);
						indicateEntityPresence(current, e, entityName);
					}
					return 0;
				}	
			}
		}else{
			//The target is not reachable from the current position
			Debug.error(entityName+" - moveTo : target ["+targetedPosition+"] is not reachable from current position ["+currentPosition+"]");
			return 0;
		}

	}



	public synchronized boolean openLock(String entityName, Location location, ElementType e) {
		Objects.requireNonNull(entityName,"Agent name should not be empty");
		Objects.requireNonNull(location,"The location should not be null");
		Objects.requireNonNull(e,"The ElementType should be set");

		boolean safeIsopen=false;
		//1° Current location
		Node n= this.graph.getNode(((GsLocation)location).getLocationId());
		//2) Check that the treasure exists on this location
		if (n.hasAttribute(e.getName())){
			//check the lock status (if any)
			if (n.hasAttribute(ElementType.LOCK.getName())){
				LockElement lock=(LockElement) n.getAttribute(ElementType.LOCK.getName());
				if (lock.isOpen()){
					//lock already open
					System.out.println("Safe already open");
					safeIsopen=true;
				}else{
					// 	the safe is closed, lets try to open it
					//If calling agent possess the expertise, open it
					//otherwise collective.
					safeIsopen=lock.tryToOpen(aggregateExpertise(entityName,((GsLocation)location).getLocationId()));
				}
			}else{
				//no lock on the safe
				safeIsopen=true;
			}

		}else{
			System.out.println("There is no "+e.getName()+ " on this location or the agent type does not allow him to open the safe");	
		}
		return safeIsopen;
	}

	/**
	 * Close the lock of the safe (if any) on the current location
	 * Do not work if there are several
	 */
	public boolean closeLock(Location location) {
		Objects.requireNonNull(location,"The location should not be null");
		//1° Current location
		Node n= this.graph.getNode(((GsLocation)location).getLocationId());
		//2) Check if a safe with a lock exists on this location
		if (n.hasAttribute(ElementType.LOCK.getName())){
			LockElement lock=(LockElement) n.getAttribute(ElementType.LOCK.getName());
			lock.close();
		}else {
			return false;
		}
		return true;
	}

	/**
	 * @param entityName name of the agent in front of the treasure
	 * @param location nodeId
	 * @return the aggregated expertise of the agents forming a team around the calling one. 
	 */
	private Map<LockType, Integer> aggregateExpertise(String entityName, String location) {
		Map<LockType,Integer> agregatedExpertise=new HashMap<LockType,Integer>();
		//Set<Couple<LockType, Integer>> agentExp=this.agentsExpertise.get(entityName);
		Set<String> agents=new HashSet<String>();
		//agents.add(entityName);
		Set<String> connectedAgents= connectedAgents(this.graph.getNode(location),agents);
		for(String s:connectedAgents){
			//Set<Couple<LockType, Integer>> agentExp=this.agentsExpertise.get(s);
			for(Couple<LockType, Integer> c:this.agentsExpertise.get(s)){
				Integer temp=agregatedExpertise.get(c.getLeft());
				if (temp==null){
					temp=0;
				}
				agregatedExpertise.put(c.getLeft(),temp+c.getRight());
			}
		}
		return agregatedExpertise;
	}

	private Set<String> connectedAgents(Node n,Set<String> agents){
		Set<String> cagents=new HashSet<String>();
		String aName = "";
		cagents.addAll(agents);
		if (n.hasAttribute(EntityType.AGENT_COLLECTOR.getName())){
			aName=(String) n.getAttribute(EntityType.AGENT_COLLECTOR.getName());
		}
		if (n.hasAttribute(EntityType.AGENT_EXPLORER.getName())) {
			aName=(String) n.getAttribute(EntityType.AGENT_EXPLORER.getName());
		}
		if (n.hasAttribute(EntityType.AGENT_TANKER.getName())){		
			aName=(String) n.getAttribute(EntityType.AGENT_TANKER.getName());
		}
		if (n.hasAttribute(EntityType.WUMPUS.getName())) {
			aName=(String) n.getAttribute(EntityType.WUMPUS.getName());
		}
		/*if (n.hasAttribute(EntityType.WUMPUS_MOVER.getName())) {
			aName=(String) n.getAttribute(EntityType.WUMPUS_MOVER.getName());
		}*/
		if (!aName.equalsIgnoreCase("") && !cagents.contains(aName)){
			cagents.add(aName);

			Iterator<Node> iter= n.neighborNodes().iterator();//getNeighborNodeIterator();
			while(iter.hasNext()){
				Node n2=iter.next();
				cagents.addAll(connectedAgents(n2, cagents));
			}
		}//else stop
		return cagents;
	}

	public synchronized int pick(String agentName,Location currentPosition, ElementType e, Integer maxQuantity) {
		//Only pick if maxQuantity>0 and there exist an element of type (diamond or gold)  on the currentPosition and lock is open

		Objects.requireNonNull(currentPosition);
		Objects.requireNonNull(e);

		Validate.isTrue(e.getName()==ElementType.DIAMOND.getName() || e.getName()==ElementType.GOLD.getName());
		//Assert.assertTrue(e.getName()==ElementType.DIAMOND.getName() || e.getName()==ElementType.GOLD.getName());
		Objects.requireNonNull(maxQuantity);

		Node n= this.graph.getNode(((GsLocation)currentPosition).getLocationId());
		int pickedQuantity=0;

		//if there is a lock and its closed
		if (n.getAttribute(ElementType.LOCK.getName())!=null && !((LockElement) n.getAttribute(ElementType.LOCK.getName())).isOpen()){
			return pickedQuantity;
		}
		if (maxQuantity>0 && n.hasAttribute(e.getName())){
			//the agent can grab some of e and there is e on the current position
			Integer treasureToPick=(Integer) n.getAttribute(e.getName());
			if (maxQuantity>=treasureToPick){
				//the treasure is cleared
				pickedQuantity=treasureToPick;
				clearElementPresence(n, e);
				//temp gui
				//UpdateGuiTreasure("env", -pickedQuantity);
				switch(e) {
				case DIAMOND:
					this.aoenv.add2TreasureValue(Observation.DIAMOND, -pickedQuantity);
					break;
				case GOLD:
					this.aoenv.add2TreasureValue(Observation.GOLD, -pickedQuantity);
					break;
				default:
					break;
				}


			}else{
				//some of the treasure remains
				pickedQuantity=maxQuantity;	
				//update the env and apply the losses
				n.setAttribute(e.getName(),((Double)((treasureToPick-pickedQuantity)*(1-InGameConfigurationFile.PERCENTAGE_TREASURE_LOSS))).intValue());
				//temp gui
				switch(e) {
				case DIAMOND:
					this.aoenv.add2TreasureValue(Observation.DIAMOND, -(pickedQuantity+
							((Double)((treasureToPick-pickedQuantity)*(InGameConfigurationFile.PERCENTAGE_TREASURE_LOSS))).intValue()));
					break;
				case GOLD:
					this.aoenv.add2TreasureValue(Observation.GOLD, -(pickedQuantity+
							((Double)((treasureToPick-pickedQuantity)*(InGameConfigurationFile.PERCENTAGE_TREASURE_LOSS))).intValue()));
					break;
				default:
					break;
				}
				//UpdateGuiTreasure("env", -(pickedQuantity+((Double)((treasureToPick-pickedQuantity)*(InGameConfigurationFile.PERCENTAGE_TREASURE_LOSS))).intValue()));
			}
			//temp gui
			//UpdateGuiTreasure(agentName, pickedQuantity);

			//listener not working properly, so force update



			this.m.updateTableGui(this.aoenv);
			//			Platform.runLater(new Runnable() {
			//				@Override
			//				public void run() {
			//					m.updateTableGui(aoenv);
			//				}
			//			});

			//***

		}

		return pickedQuantity;
	}

	/**
	 * There can be only one safe of one unique type on a given location.
	 */
	public synchronized boolean dropOff(Location location, ElementType e, Integer quantity) {
		//allowed to drop	
		if (isOktoAddElementOn(e, ((GsLocation) location).getLocationId())) {
			indicateElementPresence(this.graph.getNode(((GsLocation) location).getLocationId()),e,quantity,null);
			return true;
		}
		return false;
	}


	public boolean throwGrenade(String agentName,Location targetName) {
		//TODO To implement	
		Debug.error("This method is not yet implemented");
		return false;
	}

	/**
	 * This method must be synchronized due tothe way graphStream computes the shortestPath
	 * @param senderName name of the entity willing to send the message
	 * @param receiverName name of the receiver
	 * @param communicationReach number of hops autorised to reach the targer
	 */
	public synchronized boolean isReachable(String senderName, String receiverName, int communicationReach) {
		Location senderLocation = getCurrentPosition(senderName);
		Location receiverLocation = getCurrentPosition(receiverName);

		if (senderLocation!=null && receiverLocation!=null){
			Node senderNode=this.graph.getNode(((GsLocation)senderLocation).getLocationId());
			Node receiverNod=this.graph.getNode(((GsLocation)receiverLocation).getLocationId());
			boolean bug=false;
			//System.out.println("Computed Distance from sender:"+senderName+"(node: "+senderNode+") to "+receiverName+" (node: "+receiverNod+"):");

			Dijkstra dijkstra = new Dijkstra(Element.EDGE,null,null);

			// Compute the shortest paths in g from A to all nodes
			dijkstra.init(this.graph);
			dijkstra.setSource(senderNode);
			try{
				dijkstra.compute();
			} catch (Exception e ){
				Debug.info("Bug in GrasphStream's dijkstra compute() : "+e.toString());
				bug=true;
			}
			Double dist;
			if(!bug){
				dist=dijkstra.getPathLength(receiverNod);
			}else{
				//System.err.println("\n Dijkstra");
				//for now if there is a bug, do not communicate
				dijkstra.clear();
				return false;
			}
			//System.out.println("COmm reach: "+communicationReach);
			//System.out.println("Computed Distance from sender:"+senderName+"(node: "+senderNode+") to "+receiverName+" (node: "+receiverNod+"):"+dist);
			if (dist>communicationReach){
				dijkstra.clear();
				return false;
			}
			dijkstra.clear();
			return true;
		}else{
			return false;
		}

	}


	public String getName() {
		Objects.requireNonNull(this.environmentName);
		return this.environmentName;
	}


	/*************************************
	 * 
	 * Private methods
	 * 
	 * 
	 *************************************/

	/**
	 * 
	 * @param type true creates a grid, Dorogovtsev otherwise
	 * @param size number of iteration, the greater the bigger maze.
	 * @return a new graph
	 */
	@Deprecated
	private void generateGraph(boolean isGrid,int size){
		graph=new SingleGraph("Randomly generated graph");

		Generator gen;

		if (!isGrid){
			//generate a DorogovtsevMendes environment
			gen= new DorogovtsevMendesGenerator();
		}else{
			//generate a square grid environment
			gen= new GridGenerator();
			//gen= new BarabasiAlbertGenerator(1);
		}

		gen.addSink(graph);
		gen.begin();
		for(int i=0;i<size;i++){
			gen.nextEvents();
		}
		gen.end();

		//show the node Id on the GUI
		Iterator<Node> iter=graph.iterator();
		while (iter.hasNext()){
			Node n=iter.next();
			n.setAttribute("ui.label",n.getId());
		}

		//TO comment 
		//		String fileNameFullPath="";
		//		try {
		//			FileSink fs= new FileSinkDGS();
		//			fileNameFullPath = System.getProperty("user.dir")+"/tree.dgs";
		//			//System.out.println(fileNameFullPath.substring(0,fileNameFullPath.length()-4).concat(".dgs"));
		//			fs.writeAll(graph,fileNameFullPath);
		//		} catch (IOException e4) {
		//			// TODO Auto-generated catch block
		//			e4.printStackTrace();
		//		}
		//		System.out.println("GS successfully generated. \n File written : "+fileNameFullPath);
		//return g;
	}

	/**
	 * 
	 * @param g Type of generator to use to create the environement
	 * @param params The first parameter is the size of the graph, the others, if any, are the algorithm parameters
	 */
	private void generateGraph(GsGeneratorType g,Integer ... params){
		graph=new SingleGraph("Randomly generated graph");

		Generator gen=g.init(params);

		gen.addSink(graph);
		gen.begin();
		for(int i=0;i<params[0];i++){
			gen.nextEvents();
		}
		gen.end();

		//show the node Id on the GUI
		Iterator<Node> iter=graph.iterator();
		while (iter.hasNext()){
			Node n=iter.next();
			n.setAttribute("ui.label",n.getId());
		}
	}



	/**
	 * Load a graph from file
	 * @param topologyConfigurationFilePath fullpath to the topology
	 */
	private void loadGraph(String topologyConfigurationFilePath) {

		graph = new SingleGraph("Loaded environment");


		FileSource fs = null;
		try		{
			fs = FileSourceFactory.sourceFor(topologyConfigurationFilePath);
		} catch (IOException e1){
			e1.printStackTrace();
		}
		Objects.requireNonNull(fs);
		fs.addSink(graph);
		try	{
			fs.readAll(topologyConfigurationFilePath);
		}catch (IOException e) {
			e.printStackTrace();
		}

		Iterator<Node> it = graph.iterator();
		while (it.hasNext()){
			Node n = (Node)it.next();
			if (n.hasAttribute("label")){
				//case handMade
				//System.out.println(n.getId()+" "+n.getAttribute("label").toString());
				n.setAttribute("ui.label", n.getAttribute("label").toString());
			}else {
				//case OSM
				osm=true;
				n.setAttribute("ui.label", n.getId());
			}
		}
	}


	/**
	 * Randomly add the required components to the generated environment based on the configurationFile (presence or not), the InGameCOnfiguration file and the ElemenType class
	 * Currently : well, gold, diamond
	 */
	private void addElements(boolean diamond,boolean gold,boolean well) {
		//Random r;

		if (well){
			//wells added
			int nbHole=1+(int)Math.round(this.graph.getNodeCount() *ElementType.WELL.getOccurrencePercentage());//getNodeSet().size()
			for (int i=0;i<nbHole;i++){
				String nodeID=findFreePlace(ElementType.WELL);
				if (nodeID!="-1"){
					Node n= this.graph.getNode(nodeID);
					indicateElementPresence(n,ElementType.WELL,null,null);
				}
			}
		}

		if (gold){
			//adding treasures
			int nbTreasures=1+(int)Math.round(this.graph.getNodeCount()*ElementType.GOLD.getOccurrencePercentage());//getNodeSet().size()
			//r= new Random();
			for(int i=0;i<nbTreasures;i++){
				String nodeID=findFreePlace(ElementType.GOLD);
				if (nodeID!="-1"){
					//createTreasure(nodeID,ElementType.GOLD);
					Node n= this.graph.getNode(nodeID);
					//r= new Random();
					indicateElementPresence(n,ElementType.GOLD,1+r.nextInt((Integer)InGameConfigurationFile.MAX_GOLD_VALUE),null);

				}
			}
		}

		if (diamond){
			//adding treasures
			int nbTreasures=1+(int)Math.round(this.graph.getNodeCount()*ElementType.DIAMOND.getOccurrencePercentage());//getNodeSet().size()
			//r= new Random();
			for(int i=0;i<nbTreasures;i++){
				String nodeID=findFreePlace(ElementType.DIAMOND);
				if (nodeID!="-1"){
					Node n= this.graph.getNode(nodeID);
					//r= new Random();
					indicateElementPresence(n,ElementType.DIAMOND,1+r.nextInt((Integer)InGameConfigurationFile.MAX_DIAMOND_VALUE),null);
				}
			}
		}
	}


	/**
	 * 
	 * @param fullPath Path of the file that contains the position of all the elements that should appear on the graph
	 */
	@Deprecated
	private void loadMapElementsFromTxt(String fullPath){
		if (fullPath!=null){
			BufferedReader filereader=null;
			//TODO update Paths.get to Path.of
			try {
				filereader = Files.newBufferedReader(Paths.get(fullPath),StandardCharsets.UTF_8);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			String line = null;
			try {	
				while (filereader!=null && (line = filereader.readLine()) != null) {
					String[]l=line.split(":");
					switch (l[0]) {
					case "mapname":
						System.out.println("Loading configuration for environment "+ l[1]);
						break;
					case "well":
						//addWell(l[1]);
						indicateElementPresence(this.graph.getNode(l[1]),ElementType.WELL,null,null);
						break;
					case "gold":
						LockElement lock= new LockElement();
						lock.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.LOCKPICKING, Integer.parseInt(l[3])));
						lock.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.STRENGTH, Integer.parseInt(l[4])));
						indicateElementPresence(this.graph.getNode(l[1]),ElementType.GOLD,Integer.parseInt(l[2]),lock);
						break;
					case"diamonds":
						LockElement lock2= new LockElement();
						lock2.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.LOCKPICKING, Integer.parseInt(l[3])));
						lock2.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.STRENGTH, Integer.parseInt(l[4])));
						//addTreasure(l[1], Integer.parseInt(l[2]),envComponent.DIAMONDS);
						indicateElementPresence(this.graph.getNode(l[1]),ElementType.DIAMOND,Integer.parseInt(l[2]),lock2);
						break;
					default:
						System.err.println("Loading a configuration : This type of entry does not yet exist - "+l[0]);
						System.exit(0);
						break;
					}

				}
			} catch (IOException|ArrayIndexOutOfBoundsException e) {
				System.err.println("configurationFile not reachable or malformed");
				e.printStackTrace();
				System.exit(0);
			} finally {
				try {
					filereader.close();
				} catch (IOException e) {
				}
			}
		}
	}



	private void loadMapElementsFromJson(String fileDescribingElementsToDeploy) {
		List<ElementCharacteristicsFromJSON> listElementsCharacsForTheMap =null;
		Debug.info("Loading elements from file: "+fileDescribingElementsToDeploy);
		//JSON processing
		ObjectMapper mapper = new ObjectMapper();
		//1) We load the content of the file
		try {
			// JSON file to Java object
			listElementsCharacsForTheMap = Arrays.asList(mapper.readValue(new File(fileDescribingElementsToDeploy), ElementCharacteristicsFromJSON[].class));        	
		} catch (IOException e) {
			Debug.error("JSON configuration file describing elements on the map is not reachable or malformed: \n"+fileDescribingElementsToDeploy);
			Debug.error("Check the path and the file format (See :https://dedale.gitlab.io/page/tutorial/configureenv/)");
			e.printStackTrace();
		}
		Objects.requireNonNull(listElementsCharacsForTheMap);

		//2) For each element we add it to the map 
		System.out.println(listElementsCharacsForTheMap);
		//TODO for now we keep the same method, but we should soon use refactor ElementsTypes and add objects on the map.
		listElementsCharacsForTheMap.forEach(e -> indicateElementPresence(e));
		
	}


	/**
	 * A node is free is there is no agent nor well on it
	 * @return the nodeId of a free node, -1 if no node is available
	 **/
	private synchronized String findFreePlace(ElementType e)	{
		//Random r = new Random();
		Iterator<Node> iter = this.graph.iterator();
		List<String> emptyNode = new ArrayList<String>();
		while (iter.hasNext()){
			Node n = (Node)iter.next();
			if (isOktoDeployElementOn(e, n.getId()))
				emptyNode.add(n.getId());
		}
		if (!emptyNode.isEmpty())
			return (String)emptyNode.get(r.nextInt(emptyNode.size()));
		return "-1";
	}


	/**
	 * Check is the targeted destination is ok in order to deploy an entity on it (no wumpus, agent, well or stat node)
	 * 
	 * @param targetedNodeId
	 * @return true is the target is free
	 */

	private boolean isOkToDeployEntityOn(String targetedNodeId){
		Node n = this.graph.getNode(targetedNodeId);
		boolean free = !n.getId().contains("cat:");//nodeAsAttributeContaining(n,"cat:");
		//System.err.println("Node is a statistical one: "+!free);
		EntityType [] iter= EntityType.values();
		int i=0;
		while (i<iter.length && free){
			free=!n.hasAttribute(iter[i].getName());
			i++;
		}
		return free;
	}

	/**
	 * 
	 * @param n the node to analyse
	 * @param s : the 
	 * @return true if the node is contains at least one atttribute containing s.
	 */
	private boolean nodeAsAttributeContaining(Node n,String s) {
		return n.attributeKeys().anyMatch(a -> a.contains(s));
	}

	/**
	 * Check is the targeted destination is ok to deploy the element depending of its  type
	 * This method is called when creating the environment.
	 * @param e Type of Element to deploy
	 * @param targetedNodeId
	 * @return true if the target is free
	 */
	private boolean isOktoDeployElementOn(ElementType e,String targetedNodeId){
		Node n = this.graph.getNode(targetedNodeId);
		//Iterator<String> iter = n.getAttributeKeyIterator();
		boolean free = true;
		switch (e) {
		case DIAMOND: case GOLD:
			// ok if there is not already a well or an agent on this position
			free = ! (n.hasAttribute(ElementType.WELL.getName()) || n.hasAttribute(EntityType.AGENT_COLLECTOR.getName())|| n.hasAttribute(EntityType.AGENT_EXPLORER.getName())|| n.hasAttribute(EntityType.AGENT_TANKER.getName()) || n.hasAttribute(EntityType.WUMPUS.getName()));//|| n.hasAttribute(EntityType.WUMPUS_MOVER.getName())) ;
			break;	
		case WELL:
			// ok if there is not already a well or treasures or agents on this position
			free = ! (n.hasAttribute(ElementType.WELL.getName()) || n.hasAttribute(ElementType.DIAMOND.getName()) || n.hasAttribute(ElementType.GOLD.getName()) || n.hasAttribute(EntityType.AGENT_COLLECTOR.getName())|| n.hasAttribute(EntityType.AGENT_EXPLORER.getName())|| n.hasAttribute(EntityType.AGENT_TANKER.getName()) || n.hasAttribute(EntityType.WUMPUS.getName()));//|| n.hasAttribute(EntityType.WUMPUS_MOVER.getName())) ;
			break;
		default:
			break;
		}
		return free;
	}

	/**
	 * There can be only one safe of one unique type on a given location.
	 * there can only be a new well on an empty location
	 * This method is called when moving resources online
	 * @param e
	 * @param targetedNodeId
	 * @return true if the element can be added
	 */
	private boolean isOktoAddElementOn(ElementType e,String targetedNodeId) {

		Objects.requireNonNull(e);
		//Assert.assertNotEquals(targetedNodeId,"");
		Validate.isTrue(targetedNodeId!="");
		Node n=this.graph.getNode(targetedNodeId);
		Objects.requireNonNull(n);
		//Iterator<String> iter = n.getAttributeKeyIterator();
		boolean free = true;
		switch (e) {
		case DIAMOND:
			free = ! (n.hasAttribute(ElementType.GOLD.getName()) || n.hasAttribute(ElementType.WELL.getName())) ;
			break;
		case GOLD :
			free = ! (n.hasAttribute(ElementType.DIAMOND.getName()) || n.hasAttribute(ElementType.WELL.getName()) ) ;
			break;
		case WELL:
			free=false;
			break;
		default:
			free=false;
			Debug.error(this.getClass().getName()+" - This element should not be added "+e);
			break;
		}
		return free;
	}

	/**
	 * 
	 * @param e Element to deploy on the map
	 */
	private void indicateElementPresence(ElementCharacteristicsFromJSON e) {
		//TODO use the radius for any element
		switch (e.getElementName()) {
		case "well":
			indicateElementPresence(this.graph.getNode(e.getInitialLocation().getLocationId()),ElementType.WELL,null,null);
			break;
		case "gold":
			LockElement lock= new LockElement();
			lock.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.LOCKPICKING, e.getLockPickingExpertise()));
			lock.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.STRENGTH, e.getStrengthExpertise()));
			indicateElementPresence(this.graph.getNode(e.getInitialLocation().getLocationId()),ElementType.GOLD,e.getQuantity(),lock);
			break;
		case "diamonds":
			LockElement lock2= new LockElement();
			lock2.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.LOCKPICKING, e.getLockPickingExpertise()));
			lock2.addCriteria(new Couple<LockElement.LockType, Integer>(LockType.STRENGTH, e.getStrengthExpertise()));
			//addTreasure(l[1], Integer.parseInt(l[2]),envComponent.DIAMONDS);
			indicateElementPresence(this.graph.getNode(e.getInitialLocation().getLocationId()),ElementType.DIAMOND,e.getQuantity(),lock2);
			break;
		default:
			System.err.println("Loading a configuration : This type of entry does not yet exist - "+e);
			System.exit(0);
			break;
		}


	}

	/**
	 * 
	 * @param n the node to deploy the element (well,..)
	 * @param elem The element to add on the map
	 * @param value (null if no value related to this element)
	 * @param lock (null if no lock, otherwise LockElement)
	 * If the element is already present, the value is added.
	 * 
	 * Examples: indicateElementPresence(n,ElementType.WELL,null,null);indicateElementPresence(n,ElementType.Gold,42,null);indicateElementPresence(n,ElementType.Gold,42,LockElement);
	 * 
	 */
	private void indicateElementPresence(Node n, ElementType elem, Integer value,LockElement lock) {

		//1° get the nodes to update
		Set<Node> n2update;
		Integer i;
		//2° update them
		switch (elem) {
		case GOLD :
			i=(Integer) n.getAttribute(ElementType.GOLD.getName());
			if (i!=null){
				//if there already is gold, increment
				n.setAttribute(ElementType.GOLD.getName(),value+i);
			}else{
				n.setAttribute(ElementType.GOLD.getName(),value);
			}
			if (lock!=null){
				n.setAttribute(ElementType.LOCK.getName(), lock);
			}
			this.aoenv.add2TreasureValue(Observation.GOLD, value);
			break;
		case DIAMOND:
			i=(Integer) n.getAttribute(ElementType.DIAMOND.getName());

			if (i!=null){
				//if there already is gold, increment
				n.setAttribute(ElementType.DIAMOND.getName(),value+i);
			}else{
				n.setAttribute(ElementType.DIAMOND.getName(),value);
			}
			if (lock!=null){
				n.setAttribute(ElementType.LOCK.getName(), lock);
			}
			this.aoenv.add2TreasureValue(Observation.DIAMOND, value);
			break;
		case WELL:
			//for the eu.su.mas.dedale.env.gs.gui
			//n.addAttribute("ui.label", envComponent.WELL.getName());
			//n.setAttribute("ui.class","well");	
			if (n.hasAttribute(ElementType.DIAMOND.getName()) ||n.hasAttribute(ElementType.GOLD.getName())|| n.hasAttribute(ElementType.WELL.getName())|| n.hasAttribute(EntityType.AGENT_COLLECTOR.getName())|| n.hasAttribute(EntityType.AGENT_EXPLORER.getName())|| n.hasAttribute(EntityType.AGENT_TANKER.getName())|| n.hasAttribute(EntityType.WUMPUS.getName())) {//|| n.hasAttribute(EntityType.WUMPUS_MOVER.getName())){
				Debug.error("Impossible to deploy a Well on a position where another entity or element is. Check your configuration file");
			}else{
				n.setAttribute(ElementType.WELL.getName(),ElementType.WELL.getName());

				//for the other agents (wind,noise,...)
				n2update=findNeighbours(n,elem.getRadius());
				//n.addAttribute(ElementType.WIND.getName(),true);
				//n2update.remove(n);
				for (Node n2:n2update){
					n2.setAttribute(ElementType.WIND.getName(),true);
					updateNodeRendering(n2);
					//if its not a well, change the graphic rendering
					//if (!envComponent.WELL.getName().equalsIgnoreCase(n2.getAttribute("ui.label").toString()))
					//	n2.setAttribute("ui.class", "wind");
				}
			}
			break;
		default:
			System.out.println("This element is not yet defined");
			break;
		}
		updateNodeRendering(n);

	}


	private void indicateEntityPresence(Node n,EntityCharacteristics e, String entityName) {

		//1° get the nodes to update
		Set<Node> n2update;
		Integer i;
		//2° update them	
		switch(e.getMyEntityType()){
		case AGENT_COLLECTOR:case AGENT_EXPLORER:case AGENT_TANKER:
			//TODO use indicateElementPresence instead of this line
			n.setAttribute(e.getMyEntityType().getName(), entityName);
			break;
		case  WUMPUS : //case WUMPUS_MOVER:
			n.setAttribute(e.getMyEntityType().getName(), entityName);
			n.setAttribute(ElementType.STENCH.getName(),true);
			//			n2update=findNeighbours(n,e.getDetectionRadius());
			//			n2update.remove(n);
			//			for (Node n2:n2update){
			//				n2.setAttribute(ElementType.STENCH.getName(),true);
			//				updateNodeRendering(n2);
			//			}
			break;
		case HUMAN:
			//set the agent
			n.removeAttribute(ElementType.PATH.getName());
			n.setAttribute(e.getMyEntityType().getName(), entityName);

			//choose one direction among neighbours and colorize the path+destination node by adding the attributes on the path
			n2update=findNeighbours(n,1);
			n2update.remove(n);
			Node initiallyLightedNode=n2update.iterator().next();
			initiallyLightedNode.setAttribute(ElementType.PATH.getName(), "target");
			Edge edge=n.getEdgeBetween(initiallyLightedNode);
			edge.setAttribute(ElementType.PATH.getName(), true);
			updateNodeRendering(initiallyLightedNode);
			updateEdgeRendering(edge);
			break;
		default:
			Debug.error("The entity type does not currently exist; "+entityName+" - Type: "+e.getMyEntityType());
			break;
		}

		//from only wumpus to everyone with a detection radius > 0, should change STENCH to NOISE
		if (e.getDetectionRadius()>0) {
			n.setAttribute(ElementType.STENCH.getName(),true);
			n2update=findNeighbours(n,e.getDetectionRadius());
			n2update.remove(n);
			for (Node n2:n2update){
				n2.setAttribute(ElementType.STENCH.getName(),true);
				updateNodeRendering(n2);
			}
		}
		updateNodeRendering(n);
	}



	/**
	 * 
	 * @param n the source node
	 * @param radius the associated radius to search in
	 * @return the list of node who are reachable from n in a given radius, n included
	 */
	private Set<Node> findNeighbours(Node n, int radius) {
		Set<Node>n2update=new java.util.HashSet<Node>();

		//Non-final recursivity
		if (radius==0){
			n2update.add(n);
		}else{
			int temp=radius-1;
			n2update.add(n);
			Iterator<Node> iter=n.neighborNodes().iterator();// getNeighborNodeIterator();
			while(iter.hasNext()){
				n2update.addAll(findNeighbours(iter.next(), temp));
			}
		}
		return n2update;
	}


	/**
	 * This method is called whenever an attribute is added or removed from a node in order to update its rendering
	 * @param n node to update
	 */
	private synchronized void updateNodeRendering(Node n) {

		//first agent
		//		if (n.hasAttribute(EntityType.AGENT.getName())){
		//			n.setAttribute("ui.class", "agent");
		//			n.setAttribute("ui.label", (String)n.getAttribute(Attribute.AGENT.getName()));
		//		}else {
		if (n.hasAttribute(EntityType.AGENT_COLLECTOR.getName())){
			n.setAttribute("ui.class", "agentcollect");
			n.setAttribute("ui.label", (String)n.getAttribute(EntityType.AGENT_COLLECTOR.getName()));
		}else {
			if (n.hasAttribute(EntityType.AGENT_EXPLORER.getName())){
				n.setAttribute("ui.class", "agentexplo");
				n.setAttribute("ui.label", (String)n.getAttribute(EntityType.AGENT_EXPLORER.getName()));
			}else {
				if (n.hasAttribute(EntityType.AGENT_TANKER.getName())){
					n.setAttribute("ui.class", "agenttanker");
					n.setAttribute("ui.label", (String)n.getAttribute(EntityType.AGENT_TANKER.getName()));
				} else {
					if (n.hasAttribute(EntityType.WUMPUS.getName())){
						n.setAttribute("ui.label", (String)n.getAttribute(EntityType.WUMPUS.getName()));
						n.setAttribute("ui.class", "wumpus");
					}else{
						if (n.hasAttribute(EntityType.HUMAN.getName())) {
							n.setAttribute("ui.class", "human");
							n.setAttribute("ui.label", (String)n.getAttribute(EntityType.HUMAN.getName()));
						}else {
							//then agent potential destination
							if (n.hasAttribute(ElementType.PATH.getName())) {
								n.setAttribute("ui.class", "human");
								n.setAttribute("ui.label", "target");
							} else {
								//then gold
								if (n.hasAttribute(ElementType.GOLD.getName())){
									n.setAttribute("ui.class","gold");
									n.setAttribute("ui.label", n.getId());
								}else{
									if (n.hasAttribute(ElementType.DIAMOND.getName())){
										n.setAttribute("ui.class","diamonds");
										n.setAttribute("ui.label", n.getId());
									} else{
										//then well
										if (n.hasAttribute(ElementType.WELL.getName())){
											n.setAttribute("ui.class", "well");
											n.setAttribute("ui.label", (String)n.getAttribute(ElementType.WELL.getName()));
										}else {
											//then stench
											if (n.hasAttribute(ElementType.STENCH.getName())){
												n.setAttribute("ui.class", "stench");
												n.setAttribute("ui.label", n.getId());
											}else{
												//then wind
												if (n.hasAttribute(ElementType.WIND.getName())){
													n.setAttribute("ui.class", "wind");
													n.setAttribute("ui.label", n.getId());
												}else{
													//default node
													n.removeAttribute("ui.class");
													n.setAttribute("ui.label", n.getId());
												}
											}
										}
									}	
								}
							}
						}

						//whatever the situation is,as its not an agent, the label become the id of the node
						//n.changeAttribute("ui.label",n.getId());

					}// agent 
					//depending of the remaining attribute, the rendering class changes.
				}
			}
		}
	}


	/**
	 * This method is called whenever an attribute is added or removed from an edge in order to update its rendering
	 * @param e edge to update
	 */
	private synchronized void updateEdgeRendering(Edge e) {
		if (e.hasAttribute(ElementType.PATH.getName())) {
			e.setAttribute("ui.class", "human");
			//e.setAttribute("ui.label", n.getId());
		} else{
			e.removeAttribute("ui.class");
		}
	}
	/**
	 * Only check for free regarding an agent move, do not guarantee the safety of the targeted area (well,...)
	 * 
	 * @param targetedNodeId
	 * @return true is the entity can be moved on the target
	 */
	private boolean isPossibleToMove(String targetedNodeId) {

		Node n=this.graph.getNode(targetedNodeId);
		//Iterator<String> iter=n.getAttributeKeyIterator();
		boolean free=!(n.hasAttribute(EntityType.AGENT_COLLECTOR.getName()) || 
				n.hasAttribute(EntityType.AGENT_EXPLORER.getName()) || 
				n.hasAttribute(EntityType.AGENT_TANKER.getName()) || 
				n.hasAttribute(EntityType.WUMPUS.getName()) || 
				//n.hasAttribute(EntityType.WUMPUS_MOVER.getName())||
				n.hasAttribute(EntityType.HUMAN.getName())
				);
		return free;
	}

	/**
	 * Print the nodes and their respective attributes
	 */
	private void printAllNodes(){
		Iterator<Node> iter=this.graph.iterator();
		System.out.println("Graph content:");
		while (iter.hasNext()){
			Node n=iter.next();
			System.out.println("Node "+n.toString());
			Iterator<String> iter2=n.attributeKeys().iterator();// AttributeKeyIterator();
			while (iter2.hasNext()){
				String attributeKey=iter2.next();
				System.out.println("Attribute: "+attributeKey+";"+n.getAttribute(attributeKey).toString());
			}
		}
	}

	/***********************************************
	 * 
	 * Temp methods for GUi rendering com and aggreg
	 * 
	 * 
	 *************************************/

	//keep
	//	private void addAndInitNodeInterface()
	//	{
	//		Node n1 = this.graph.addNode("cat: T-Team");
	//		n1.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(0) });
	//		n1.setAttribute("ui.label", new Object[] { "cat: T-Team: " + n1.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		Node n2 = this.graph.addNode("cat: T-Golem");
	//		n2.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(0) });
	//		n2.setAttribute("ui.label", new Object[] { "cat: T-Golem: " + n2.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		this.graph.addEdge("a", n1, n2);
	//
	//		n1 = n2;
	//
	//		n2 = this.graph.addNode("cat: T-Silo");
	//		n2.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(0) });
	//		n2.setAttribute("ui.label", new Object[] { "cat: T-Silo: " + n2.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		this.graph.addEdge("b", n1, n2);
	//
	//		n1 = n2;
	//
	//		n2 = this.graph.addNode("cat: T-Env");
	//		this.agentCurrentGold.put("env",Integer.valueOf(computeMapGlobalGoldValue()));
	//		n2.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(computeMapGlobalGoldValue() ) });
	//		n2.setAttribute("ui.label", new Object[] { "cat: T-Env: " + n2.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		this.graph.addEdge("c", n1, n2);
	//
	//		n1 = n2;
	//
	//		/**
	//		 * The communication are now within the GUI, so we can remove this node
	//		 */
	//		//n2 = this.graph.addNode("cat: Com-nbMsgs");
	//		//n2.setAttribute(ElementType.COMMUNICATION.getName(), new Object[] { Integer.valueOf(0) });
	//		//n2.setAttribute("ui.label", new Object[] { "cat: Com-nbMsgs: " + n2.getAttribute(ElementType.COMMUNICATION.getName()).toString() });
	//
	//		//this.graph.addEdge("d", n1, n2);
	//	}

	//	// update the hashmap
	//	@Deprecated //This is now done silently
	//	public synchronized void UpdateGuiTreasure(String agentName,Integer val){
	//		Integer currentVal=this.agentCurrentGold.get(agentName);
	//		if (currentVal!=null) {
	//			this.agentCurrentGold.put(agentName, currentVal+val);
	//		}else {
	//			this.agentCurrentGold.put(agentName, val);
	//		}
	//
	//		updateTreasureRendering();
	//	}


	//Display the hashMap to the screen
	//	private synchronized void updateTreasureRendering() {
	//		//env
	//		Node nodeEnvTreasure = this.graph.getNode("cat: T-Env");
	//		nodeEnvTreasure.setAttribute(ElementType.GOLD.getName(),new Object[] {this.agentCurrentGold.get("env")});
	//		nodeEnvTreasure.setAttribute("ui.label", new Object[] { "cat: T-Env: " + nodeEnvTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		//golem
	//		Node nodeGolemTreasure = this.graph.getNode("cat: T-Golem");
	//		nodeGolemTreasure.setAttribute(ElementType.GOLD.getName(), new Object[] {(this.agentCurrentGold.get("Golem")==null)?0:this.agentCurrentGold.get("Golem")});
	//		nodeGolemTreasure.setAttribute("ui.label", new Object[] { "cat: T-Golem: " + nodeGolemTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		//Silo
	//		Node nodeTankerTreasure = this.graph.getNode("cat: T-Silo");
	//		nodeTankerTreasure.setAttribute(ElementType.GOLD.getName(), new Object[] {(this.agentCurrentGold.get("Silo")==null)?0:this.agentCurrentGold.get("Silo")});
	//		nodeTankerTreasure.setAttribute("ui.label", new Object[] { "cat: T-Silo: " + nodeTankerTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		///Team
	//		Node nodeTeam = this.graph.getNode("cat: T-Team");
	//		Integer aggreg=0;
	//		for (String agent : this.agentCurrentGold.keySet()) {
	//			if (!(agent.equalsIgnoreCase("env") || agent.equalsIgnoreCase("Golem") || agent.equalsIgnoreCase("Silo"))){
	//				aggreg=aggreg+this.agentCurrentGold.get(agent);
	//			}
	//		};
	//
	//		nodeTeam.setAttribute(ElementType.GOLD.getName(), new Object[] { aggreg});
	//		nodeTeam.setAttribute("ui.label", new Object[] { "cat: T-Team: " + nodeTeam.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//	}

	private int computeMapGlobalGoldValue()
	{
		int treasureCpt = 0;

		Iterator<Node> iter = this.graph.iterator();//getNodeIterator();
		while (iter.hasNext())
		{
			Node n = (Node)iter.next();
			if ((!n.getId().startsWith("cat:")) && (n.hasAttribute(ElementType.GOLD.getName()))) {
				treasureCpt += ((Integer)n.getAttribute(ElementType.GOLD.getName())).intValue();
			}
		}
		return treasureCpt;
	}
	//
	//	private int computeMapGlobalDiamondsValue()
	//	{
	//		int treasureCpt = 0;
	//		Iterator<Node> iter = this.graph.getNodeIterator();
	//		while (iter.hasNext())
	//		{
	//			Node n = (Node)iter.next();
	//			if ((!n.getId().startsWith("cat:")) && (n.hasAttribute(ElementType.DIAMOND.getName()))) {
	//				treasureCpt += ((Integer)n.getAttribute(ElementType.DIAMOND.getName())).intValue();
	//			}
	//		}
	//		return treasureCpt;
	//	}

	//	private synchronized void updateNodeInterfaceTreasureRendering(String agentName, int grabbed, int disapearing)
	//	{
	//		Node nodeEnvTreasure = this.graph.getNode("cat: T-Env");
	//		nodeEnvTreasure.addAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeEnvTreasure.getAttribute(ElementType.GOLD.getName())).intValue() - disapearing) });
	//		nodeEnvTreasure.addAttribute("ui.label", new Object[] { "cat: T-Env: " + nodeEnvTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//		if (agentName.contains("Golem"))
	//		{
	//			Node nodeGolemTreasure = this.graph.getNode("cat: T-Golem");
	//			nodeGolemTreasure.addAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeGolemTreasure.getAttribute(ElementType.GOLD.getName())).intValue() + grabbed) });
	//			nodeGolemTreasure.addAttribute("ui.label", new Object[] { "cat: T-Golem: " + nodeGolemTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//		}
	//		else
	//		{
	//			Node nodeTeamTreasure = this.graph.getNode("cat: T-Team");
	//			nodeTeamTreasure.addAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeTeamTreasure.getAttribute(ElementType.GOLD.getName())).intValue() + grabbed) });
	//			nodeTeamTreasure.addAttribute("ui.label", new Object[] { "cat: T-Team: " + nodeTeamTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//		}
	//	}

	//	private synchronized void updateNodeInterfaceTreasureRenderingDroppOff(String agentName, int dropped)
	//	{
	//		Node nodeEnvTreasure = this.graph.getNode("cat: T-Env");
	//		nodeEnvTreasure.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeEnvTreasure.getAttribute(ElementType.GOLD.getName())).intValue() + dropped) });
	//		nodeEnvTreasure.setAttribute("ui.label", new Object[] { "cat: T-Env: " + nodeEnvTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		Node nodeGolemTreasure = this.graph.getNode("cat: T-Golem");
	//		nodeGolemTreasure.setAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeGolemTreasure.getAttribute(ElementType.GOLD.getName())).intValue() - dropped) });
	//		nodeGolemTreasure.setAttribute("ui.label", new Object[] { "cat: T-Golem: " + nodeGolemTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//	}

	//	private synchronized void updateNodeInterfaceTreasuresRenderingSilo(String agentName, int valueTransfered)
	//	{
	//		Node nodeTeam = this.graph.getNode("cat: T-Team");
	//		nodeTeam.addAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)nodeTeam.getAttribute(ElementType.GOLD.getName())).intValue() - valueTransfered) });
	//		nodeTeam.addAttribute("ui.label", new Object[] { "cat: T-Team: " + nodeTeam.getAttribute(ElementType.GOLD.getName()).toString() });
	//
	//		Node nodeTankerTreasure = this.graph.getNode("cat: T-Silo");
	//		nodeTankerTreasure.addAttribute(ElementType.GOLD.getName(), new Object[] { Integer.valueOf(((Integer)this.agentCurrentDiamond.get(agentName)).intValue() + ((Integer)this.agentCurrentGold.get(agentName)).intValue()) });
	//		nodeTankerTreasure.addAttribute("ui.label", new Object[] { "cat: T-Silo: " + nodeTankerTreasure.getAttribute(ElementType.GOLD.getName()).toString() });
	//	}

	public synchronized void updateNodeInterfaceComRendering(Integer val)
	{
		Node n = this.graph.getNode("cat: Com-nbMsgs");
		n.setAttribute(ElementType.COMMUNICATION.getName(), new Object[] { Integer.valueOf(((Integer)n.getAttribute(ElementType.COMMUNICATION.getName())).intValue() + val) });
		n.setAttribute("ui.label", new Object[] { "cat: Com-nbMsgs: " + n.getAttribute(ElementType.COMMUNICATION.getName()).toString() });
	}


	/**
	 * Update the nb of messages sent and effectively transmitted
	 * @param nbSent number of messages sent
	 * @param nbReceived number of messages received (as the messages can be losts if agents are out of reach)
	 */
	public synchronized void updateCommunicationStats(int nbSent,int nbReceived) {

		//m.setSentMessages(nbSent);
		//m.setReceivedMessages(nbReceived);

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				m.addToSentMessages(nbSent);
				m.addToTransmittedMessages(nbReceived);
			}
		});
	}

	public synchronized void updateAgentsStatsWithinGui() {
		this.m.updateTableGui(aoenv);
	}

	/**
	 * 
	 * @return the (local)name of the agent the user wants to terminate
	 */
	public synchronized String getAgentToKill() {
		return (m==null?null:this.m.getFirstToKill());
	}

	/**
	 * 
	 * @param agentName
	 * @param currentPosition
	 * @return the current next node of the user-controlled agent according to the graph (and the GUI)
	 */
	private String getProposedPath(String agentName,String currentPosition) {

		Set<Node> neighbours=this.findNeighbours(this.graph.getNode(currentPosition),1);
		neighbours.remove(this.graph.getNode(currentPosition));
		//Node dest=neighbours.stream().filter(n -> n.getAttribute("ui.class")=="human").findAny().get();
		Node dest=neighbours.stream().filter(n -> n.hasAttribute(ElementType.PATH.getName())).findAny().get();

		Objects.requireNonNull(dest);
		return dest.getId();
	}

	/**
	 * Test javaFx GS rendering
	 * @return JavaFxViewPanel of a random graph
	 */
	private FxViewPanel truc(){
		MultiGraph graph = new MultiGraph("mg");//createGsGraph();
		FxViewer viewer = new FxViewer(graph, FxViewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		viewer.enableAutoLayout();
		//FxGraphRenderer renderer = new FxGraphRenderer();
		//FxViewPanel panel = (FxViewPanel)viewer.addDefaultView(false, renderer);


		DorogovtsevMendesGenerator gen = new DorogovtsevMendesGenerator();

		gen.addSink(graph);
		gen.begin();
		for(int i = 0 ; i < 100 ; i++)
			gen.nextEvents();
		gen.end();
		gen.removeSink(graph);


		graph.setAttribute("ui.antialias");
		graph.setAttribute("ui.quality");
		graph.setAttribute("ui.stylesheet", STYLE_SHEET);


		//FxViewPanel panel = (FxViewPanel)viewer.addDefaultView(false, new FxGraphRenderer());
		FxViewPanel panel = (FxViewPanel)viewer.addDefaultView(false, new FxGraphRenderer());

		return panel;
	}

	/**
	 * javaFx GS rendering
	 * @return JavaFxViewPanel of the graph to be integrated within the application's GUI
	 */
	private FxViewPanel getJavaFxViewer(){

		FxViewer viewer = new FxViewer(graph, FxViewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		if (!osm) {
			viewer.enableAutoLayout();

		}else {
			//osm
			viewer.disableAutoLayout();
		}

		graph.setAttribute("ui.antialias");
		graph.setAttribute("ui.quality");

		FxViewPanel panel = (FxViewPanel)viewer.addDefaultView(false, new FxGraphRenderer());

		return panel;
	}


	/***********************************************
	 *  PUBLIC METHODS NOT PART OF THE API TO DEAL WITH USER CONTROL and GUI UPDATING
	 *****************************************/
	/**
	 * 
	 * @return Return the object used to the observer of the agent state and sent to the GUI for stat updating
	 */
	public ObserverOfAgents getOoa() {
		return ooa;
	}

	/**
	 * 
	 * @return Method used by the user-controlled agent to connect him to the events the gui receives (to move the agent)
	 */
	public MyController getGuiController() {
		return this.m;
	}

	/**
	 * 
	 * @param agentName name of the agent controlled by the user
	 * @return the node currently proposed as next destination to the user
	 */
	public synchronized Location getUserChoosenPath(String agentName) {
		return new GsLocation(this.getProposedPath(agentName,((GsLocation)this.getCurrentPosition(agentName)).getLocationId()));	
	}


	/**
	 * When the user select next, we update the proposed path in incremental order of the neighbours id (whatever the sorting algo)
	 * 
	 * @param agentName name of the agent controlled by the user
	 */
	public synchronized void updateUserChoosenPath(String agentName) {
		Location currentLocation = this.getCurrentPosition(agentName);
		Node currentNodePosition=this.graph.getNode(((GsLocation)currentLocation).getLocationId());
		Set<Node> neighbourNodes=this.findNeighbours(this.graph.getNode(((GsLocation)currentLocation).getLocationId()),1);
		neighbourNodes.remove(currentNodePosition);
		String currentProposedPath=this.getProposedPath(agentName,((GsLocation)currentLocation).getLocationId());
		//Now we need to remove current edge and node then update the new ones.

		Node n=this.graph.getNode(currentProposedPath);
		n.removeAttribute(ElementType.PATH.getName());
		updateNodeRendering(n);
		Edge e=currentNodePosition.getEdgeBetween(currentProposedPath);
		e.removeAttribute(ElementType.PATH.getName());
		updateEdgeRendering(e);

		//get the next
		List<String> an=  (neighbourNodes.stream().map(x-> x.getId())).collect(Collectors.toList());

		int posit=an.indexOf(currentProposedPath);
		String futurProposedPath;
		if (posit<an.size()-1) {
			futurProposedPath=an.get(posit+1);
		}else {
			futurProposedPath=an.get(0);
		}
		Objects.requireNonNull(futurProposedPath);
		n=this.graph.getNode(futurProposedPath);
		n.setAttribute(ElementType.PATH.getName(), "target");
		updateNodeRendering(n);
		e=currentNodePosition.getEdgeBetween(futurProposedPath);
		e.setAttribute(ElementType.PATH.getName(), true);
		updateEdgeRendering(e);
	}

}