package eu.su.mas.dedale.princ;

import eu.su.mas.dedale.env.EnvironmentType;
import eu.su.mas.dedale.env.GeneratorType;

/**
 * 
 * @author hc
 *
 */
public final class ConfigurationFile {


	/************************************
	 ***********************************
	 *
	 * 1) Network and platform parameters
	 * 
	 ***********************************/

	public static boolean platformIsDistributed= false;
	public static boolean computerIsMain= true;

	public static String platformHostname="127.0.0.1";
	public static String platformID="Ithaq";

	public static Integer platformPort=8887;

	public static String localContainerName=platformID+"_"+"container1";
	public static String localContainer2Name=platformID+"_"+"container2";
	public static String localContainer3Name=platformID+"_"+"container3";
	public static String localContainer4Name=platformID+"_"+"container4";


	/************************************
	 ************************************
	 *
	 * 2) Environment parameters 
	 * 
	 ************************************/
	
	/**
	 * 	The GateKeeper is in charge of the Platform and of the agents within, do not change its name.
	 */
	public static String defaultGatekeeperName="GK";

	/**
	 * The environment is either based on GraphStream (2D discrete) or JME (3D continuous).
	 */
	public static EnvironmentType environmentType=EnvironmentType.GS;
	
	/**
	 * The environment is either manually designed (manual), or generated with a specific generator (dorogovstev, grid,barabasi)
	 */
	public static GeneratorType generatorType=GeneratorType.MANUAL;

	
	

	/************************************
	 *
	 * 2-a) Environment parameters IF manually designed
	 * 
	 ************************************/

	/**
	 * Give the topology
	 */
	//public static String INSTANCE_TOPOLOGY=null;
//	public static String INSTANCE_TOPOLOGY="src/test/java/resources/map2024-topology-easyHunt";//mapFloodEcho-topology
//	public static String INSTANCE_TOPOLOGY="src/test/java/resources/map2024-topologyExam2-s.dgs";
	public static String instanceTopology="src/test/java/resources/map2018-topology";
	//public static String INSTANCE_TOPOLOGY="src/test/java/resources/testJussieu5.dgs";//aquila2";//CentreParis4.dgs";//map2019-topologyExam1";
	//public static String INSTANCE_TOPOLOGY="src/test/java/resources/HouatTopology";


	/**
	 * Give the elements available on the map, if any
	 */
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/emptyMap";
	//public static String instanceConfigurationElements="src/test/java/resources/map2018-elements";
	public static String instanceConfigurationElements="src/test/java/resources/map2018-elements.json";
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/map2019-elementsExam1";
	//public static String INSTANCE_CONFIGURATION_ELEMENTS="src/test/java/resources/Houat-elements";


	/************************************
	 * 
	 * 
	 * 2-b) Environment parameters IF the environment is generated 
	 * 
	 * 
	 ***********************************/

	/**
	 * Size of the generated environment 
	 */
	public static Integer environmentSize=3;
	public static Integer optionalAdditionalEnvGeneratorParam1=1;//used by the BARABASI_ALBERT generator to control the branching factor of the graph 
	public static Integer[] generatorParameters= {environmentSize,optionalAdditionalEnvGeneratorParam1};

	
	/**
	 * Wumpus proximity detection radius when generated
	 */
	public static final Integer DEFAULT_DETECTION_RADIUS = 1;


	/**
	 * 	Agents communication radius when generated
	 */
	public static final Integer DEFAULT_COMMUNICATION_REACH=3;

	/**
	 * Elements on the map when generated
	 */
	
	public static final boolean ACTIVE_WELL=false;
	public static final boolean ACTIVE_GOLD=true;
	public static final boolean ACTIVE_DIAMOND=false;

	
	/************************************
	 ************************************
	 *
	 * 3) Agents characteristics
	 * 
	 ************************************/

	//public static String instanceConfigurationEntities=null;
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentsMap1-2024.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2024-2-entities.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentExplo";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExplo-2";
	public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExplo-2.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExploSolo";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentExploCoop-2";	
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentTanker";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentTankerTest.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentCollect";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-entities";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentGolem";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentKeyboardControlled";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentKeyboardControlled.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentTankerCollect";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2019-entitiesExam1";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/agentObserveTest.json";
	//public static String INSTANCE_CONFIGURATION_ENTITIES="src/test/java/resources/map2018-agentCollect.json";
}
