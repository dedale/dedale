package eu.su.mas.dedale.gui;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.graphstream.ui.fx_viewer.FxViewPanel;

import eu.su.mas.dedale.mas.agent.knowledge.AgentObservableElement;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.DataStore;
import jade.util.InputQueue;
import javafx.fxml.FXML;
import java.awt.Desktop;

public class MyController {

	private String keyPressed="";
	private boolean iskeyPressed=false;
	private KeyboardObservable keyPressedObserved= new KeyboardObservable();
	
	/**
	 * This is used as a link between the Gk and the GUI to identify the agents to kill
	 */
	private InputQueue killList;
	

	// When user click on myButton
	// this method will be called.

	//Game;
	@FXML private MenuItem configure;
	@FXML private MenuItem exit;

	//Map
	@FXML private MenuItem loadtopology;
	@FXML private MenuItem createtopoloy;
	@FXML private MenuItem savetopology;

	//Help;
	@FXML private MenuItem forum;
	@FXML private MenuItem tchat;
	@FXML private MenuItem website;
	@FXML private MenuItem about;

	//right pannel
	@FXML private AnchorPane right;

	//leftpanel
	/**
	 * Label related to communication
	 * 
	 * nbSent > nbReceived as non reachable agents --> lost messages
	 */

	@FXML private Label msgSent;
	@FXML private Label msgReceived;
	@FXML private TableView<AgentObservableElement> observeTab;


	/**************
	 * Game menu
	 *************/
	@FXML
	private void dedaleConfigure(ActionEvent event) {

	}

	@FXML
	private void dedaleStart(ActionEvent event) {

	}	

	@FXML
	private void handleExitAction(ActionEvent event) {
		System.out.println("exiting");
		System.exit(0);
		//Platform.exit();
	}

	/**@FXML
	public void dedaleHelp(ActionEvent event){
		System.out.println("Dedale site");
		openUrl("https://dedale.gitlab.io/");
		System.out.println("Post");
	}
	 **/


	/**************
	 * Map menu
	 *************/

	@FXML
	private void dedaleLoadTopo(ActionEvent event) {
		System.err.println("Load a topology");
		System.err.println("Not yet implemented through the GUI but you can do it in the configuration file");
	}

	@FXML
	private void dedaleCreateTopo(ActionEvent event) {
		System.err.println("Create a topology");
		System.err.println("Not yet implemented through the GUI but you can do it in the configuration file");
		openUrl("https://dedale.gitlab.io/page/tutorial/configureenv/");
	}	

	@FXML
	private void dedaleSaveTopo(ActionEvent event) {
		System.err.println("Not yet implemented through the GUI");
	}



	/**************
	 * Help menu
	 *************/


	@FXML
	public void dedaleAbout(ActionEvent event){
		System.out.println("About Dedale");
		openUrl("https://dedale.gitlab.io/page/about/");
	}

	@FXML
	public void dedaleWebsite(ActionEvent event){
		System.out.println("Dedale website");
		openUrl("https://dedale.gitlab.io/");
	}

	@FXML
	public void dedaleTchat(ActionEvent event){
		System.out.println("Discord server");
		openUrl("https://discord.gg/JZVz6sR");
	}

	@FXML
	public void dedaleForum(ActionEvent event){
		System.out.println("Dedale forum");

	}



	/**
	 * Open a browser tab with the uri given in parameter. Launch a new thread to be javafx compliant.
	 * @param uri
	 */
	private void openUrl(String uri) {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			new Thread(() -> {
				try {
					Desktop.getDesktop().browse(new URI(uri));
				} catch (IOException | URISyntaxException e) {
					e.printStackTrace();
				}
			}).start();
		}
	}

	/**
	 * Trying to add the graph ref to the scene
	 * @param viewpanel ref to the panel we add the graph into
	 */
	@SuppressWarnings("restriction")
	public synchronized void setGraph(FxViewPanel viewpanel) {
		System.out.println(viewpanel.scaleShapeProperty());
		right.getChildren().add(viewpanel);
		viewpanel.setScaleShape(true);
		viewpanel.prefWidthProperty().bind(right.widthProperty());
		viewpanel.prefHeightProperty().bind(right.heightProperty());		
	}


	/***************************
	 * 		MAS state feedback
	 ***************************/

	/**
	 * Add a given number of messages to the interface counter for sent messages
	 * @param n number of messages to add
	 */
	public synchronized void addToSentMessages(int n) {
		String[] tmp=msgSent.getText().split("=");
		int n1=Integer.parseInt(tmp[1].replace(" ",""))+n;
		msgSent.setText("nbSent = "+n1);
	}

	/**
	 * Add a given number of messages to the interface counter for received messages
	 * @param n nb messages to add
	 */
	public synchronized void addToTransmittedMessages(int n) {
		String[] tmp=msgReceived.getText().split("=");
		int n1=Integer.parseInt(tmp[1].replace(" ",""))+n;
		msgReceived.setText("nbReceived = "+n1);
	}

	public synchronized void connectGuiWithAgentObservable(AgentObservableElement e) {

		this.observeTab.getItems().add(e);
		//ol.add(e);
		//ObservableList<AgentObservableElement>ol1=	FXCollections.observableArrayList(e);
		//ObservableList<AgentObservableElement> ol2=	FXCollections.concat(ol,ol1);
		//this.observeTab.setItems(ol1);
	}

	/**
	 * Method used to bypass a non understood bug.
	 * The listener cascade (agent 2 gs ooa 2 gui) is not working for the GUI step for an unknown reason.
	 * The Gui seems not to receive the changes in ooa. 
	 * But when I force a change in the list everything is updated. Which means the links are ok, just the links are ok.
	 * The trigger are just not executed.
	 * 
	 * @param e Observable element to update for agent
	 */
	public synchronized void updateTableGui(AgentObservableElement e) {
		ObservableList<AgentObservableElement>ol=this.observeTab.getItems();
		ol.set(ol.indexOf(e),e);
	}


	public synchronized void init() {
		//this.observeTab = new TableView();
		killList = new InputQueue();
		
		System.out.println(this.observeTab);
		TableColumn<AgentObservableElement, String> column1 = new TableColumn<>("Agent");
		column1.setCellValueFactory(new PropertyValueFactory<>("agentName"));

		TableColumn<AgentObservableElement, Integer> column2 = new TableColumn<>("Gold");
		column2.setCellValueFactory(new PropertyValueFactory<>("currentGoldValue"));

		TableColumn<AgentObservableElement, Integer> column3 = new TableColumn<>("Diamond");
		column3.setCellValueFactory(new PropertyValueFactory<>("currentDiamondValue"));

		// Set Sort type for userName column
		column1.setSortType(TableColumn.SortType.ASCENDING);


		observeTab.getColumns().add(column1);
		observeTab.getColumns().add(column2);
		observeTab.getColumns().add(column3);

		// Add buttons
		addKillSwitchButtonToTable();
		
	}

	/**
	 * Add button to the tableView and trigger action when selected
	 */
	private void addKillSwitchButtonToTable() {
		
		//Kill-Switch column
		TableColumn<AgentObservableElement, Void> colBtnKillSwitch = new TableColumn("KillSwitch");
		Callback<TableColumn<AgentObservableElement, Void>, TableCell<AgentObservableElement, Void>> cellFactory = new 
				Callback<TableColumn<AgentObservableElement, Void>, TableCell<AgentObservableElement, Void>>() {

			@Override
			public TableCell<AgentObservableElement, Void> call(final TableColumn<AgentObservableElement, Void> param) {
				final TableCell<AgentObservableElement, Void> cell = new TableCell<AgentObservableElement, Void>() {
					private final Button btn = new Button("Terminate");
					{
						btn.setOnAction((ActionEvent event) -> {
							AgentObservableElement data = getTableView().getItems().get(getIndex());
							//System.out.println("selectedData: " + data);
							//System.out.println("Agent local-name: "+data.getAgentName());
							if (data.getAgentName()!="env") {
								killList.put(data.getAgentName());	
							}
						});
					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		colBtnKillSwitch.setCellFactory(cellFactory);

		observeTab.getColumns().add(colBtnKillSwitch);

	}

	/***********************
	 * User controlled agent
	 ***********************/

	@FXML
	public synchronized void keyPressed(KeyEvent keyEvent) {
		switch (keyEvent.getCode()) {
		case N:
			System.out.println("The key N is pressed");

			this.iskeyPressed=true;
			this.keyPressed="N";
			this.keyPressedObserved.setKeyPressed("N");
			break;
		case O:
			System.out.println("The key O is pressed");
			this.iskeyPressed=true;
			this.keyPressed="O";
			this.keyPressedObserved.setKeyPressed("O");
			break;
		default:
			break;
		}
	}

	public KeyboardObservable getKeyObservable() {
		return this.keyPressedObserved;
	}

	/**
	 * Associate the agent behaviour in charge of the killing to this list
	 * @param b behaviour to add
	 */
	public void linkBehaviourToKlist(Behaviour b) {
		this.killList.associate(b);
	}
	
	/**
	 * 
	 * @return null if there is no agent to kill, agentLocalName otherwise
	 */
	public String getFirstToKill() {
		return (String) this.killList.get();
	}
	
}