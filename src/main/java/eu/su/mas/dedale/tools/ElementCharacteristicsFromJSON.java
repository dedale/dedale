package eu.su.mas.dedale.tools;

import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.gs.GsLocation;

/**
 * Used to automatically parse an element to instantiate on the map from JSON
 */
public class ElementCharacteristicsFromJSON {

	private String elementName;
	private Location initialLocation;
	private Integer detectionRadius; //0 if only visible on the node
	private Integer quantity; //0 if well
	private Integer lockPickingExpertise; //0 if well
	private Integer strengthExpertise; //0 if well
	
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public Location getInitialLocation() {
		return initialLocation;
	}
	
	public void setInitialLocation(String initialLocation) {
		this.initialLocation = new GsLocation(initialLocation);
	}	
	
	public Integer getDetectionRadius() {
		return detectionRadius;
	}
	public void setDetectionRadius(Integer detectionRadius) {
		this.detectionRadius = detectionRadius;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getLockPickingExpertise() {
		return lockPickingExpertise;
	}
	public void setLockPickingExpertise(Integer lockPickingExpertiseRequired) {
		this.lockPickingExpertise = lockPickingExpertiseRequired;
	}
	public Integer getStrengthExpertise() {
		return strengthExpertise;
	}
	public void setStrengthExpertise(Integer strengthExpertiseRequired) {
		this.strengthExpertise = strengthExpertiseRequired;
	}
	
	public String toString() {
		return " elementType = "+elementName+
				"\n initialLocation = "+initialLocation+
				"\n detectionRadius = "+detectionRadius+
				"\n quantity = "+quantity+				
				"\n requiredStrengthExpertise = "+strengthExpertise+
				"\n requiredLockPickingExpertise =" +lockPickingExpertise+"\n";
	}
	


}
