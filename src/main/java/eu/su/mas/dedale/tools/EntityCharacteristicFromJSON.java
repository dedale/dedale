package eu.su.mas.dedale.tools;

import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.gs.GsLocation;

/**
 * Used to automatically load and parse the characteristics of an agent.
 * See AbstractDedaleAgent.loadEntityCharacteristicsFromJson
 */
public class EntityCharacteristicFromJSON {

	private String agentType;
	private String agentName;
	private Integer communicationRange;
	private Location initialLocation;
	private Integer backPackCapacityGold;
	private Integer backPackCapacityDiamond;
	private Integer detectionRadius;
	private Integer strengthExpertise;
	private Integer lockPickingExpertise;

//	public EntityCharacteristicFromJSON() {
//
//	}
//
//	public EntityCharacteristicFromJSON(String agentType,String agentName,Integer communicationRange,String initialLocation, 
//			Integer backPackCapacityGold,Integer backPackCapacityDiamond,Integer detectionRadius,
//			Integer strengthExpertise,Integer lockPickingExpertise) {
//		this.agentName=agentName;
//		this.agentType=agentType;
//		this.backPackCapacityDiamond=backPackCapacityDiamond;
//		this.backPackCapacityGold=backPackCapacityGold;
//		this.communicationRange=communicationRange;
//		this.initialLocation=initialLocation;
//		this.detectionRadius=detectionRadius;
//		this.lockPickingExpertise=lockPickingExpertise;
//		this.strengthExpertise=strengthExpertise;
//	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Integer getCommunicationRange() {
		return communicationRange;
	}

	public void setCommunicationRange(Integer communicationRange) {
		this.communicationRange = communicationRange;
	}

	public Location getInitialLocation() {
		return initialLocation;
	}

	public void setInitialLocation(String initialLocation) {
		this.initialLocation = new GsLocation(initialLocation);
	}

	public Integer getBackPackCapacityGold() {
		return backPackCapacityGold;
	}

	public void setBackPackCapacityGold(Integer backPackCapacityGold) {
		this.backPackCapacityGold = backPackCapacityGold;
	}

	public Integer getBackPackCapacityDiamond() {
		return backPackCapacityDiamond;
	}

	public void setBackPackCapacityDiamond(Integer backPackCapacityDiamond) {
		this.backPackCapacityDiamond = backPackCapacityDiamond;
	}

	public Integer getDetectionRadius() {
		return detectionRadius;
	}

	public void setDetectionRadius(Integer detectionRadius) {
		this.detectionRadius = detectionRadius;
	}

	public Integer getStrengthExpertise() {
		return strengthExpertise;
	}

	public void setStrengthExpertise(Integer strengthExpertise) {
		this.strengthExpertise = strengthExpertise;
	}

	public Integer getLockPickingExpertise() {
		return lockPickingExpertise;
	}

	public void setLockPickingExpertise(Integer lockPickingExpertise) {
		this.lockPickingExpertise = lockPickingExpertise;
	}	
	
	public String toString() {
		return agentName+" \n"+
				" agentType = "+agentType+
				"\n communicationRange = "+ communicationRange+
				"\n initialLocation = "+initialLocation+
				"\n backPackCapacityGold = "+backPackCapacityGold+
				"\n backPackCapacityDiamond = "+backPackCapacityDiamond+
				"\n detectionRadius = "+detectionRadius+
				"\n strengthExpertise = "+strengthExpertise+
				"\n lockPickingExpertise =" +lockPickingExpertise+"\n";
	}
	
	
}
