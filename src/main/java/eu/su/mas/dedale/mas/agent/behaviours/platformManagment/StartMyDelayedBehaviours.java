package eu.su.mas.dedale.mas.agent.behaviours.platformManagment;

import java.util.List;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.WakerBehaviour;

/**
 * Start behaviours after a given time
 * @author hc
 *
 */
public class StartMyDelayedBehaviours extends WakerBehaviour {
	private static final long serialVersionUID = 2439126938797533151L;
	private List<Behaviour> lBehav;

	/**
	 * Start behaviours after a given time
	 * 
	 * @param a the agent
	 * @param behaviourList the list of behaviours to start after the timeout
	 * @param timeout duration (in ms)
	 */
	public StartMyDelayedBehaviours(Agent a, List<Behaviour> behaviourList,long timeout) {
		super(a, timeout);
		lBehav=behaviourList;

	}

	public void onWake() {
		for (Behaviour b:lBehav){
			//this.myAgent.addBehaviour(new RandomShiftBehaviour((AbstractDedaleAgent) this.myAgent));
			this.myAgent.addBehaviour(b);
		}
	}


}
