package eu.su.mas.dedale.mas.agent.knowledge;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import debug.Debug;
import eu.su.mas.dedale.env.Observation;

/**
 * All the elements of an agent but will be made observable for the team's COS GUI + GK + log
 * See test/observables/testPCL for example.
 * @author hc
 *
 */
public class AgentObservableElement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6974622317205097092L;
	
	private String agentName;
	private int currentGoldValue;
	private int currentDiamondValue;
	private int nbMsgSent;
	private int nbMsgSentAndDelivered;
	private int nbMsgReceived;

	private PropertyChangeSupport support;

	/**
	 * Initialise the observable properties of an agent
	 * @param agentName localname of the agent
	 */
	public AgentObservableElement(String agentName) {
		this.agentName=agentName;
		currentDiamondValue=0;
		currentGoldValue=0;
		nbMsgReceived=0;
		nbMsgSentAndDelivered=0;
		nbMsgSent=0;

		support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public void setCurrentGoldValue(int goldValue) {

		support.firePropertyChange(this.agentName+";"+"gold", this.currentGoldValue, goldValue);
		this.currentGoldValue = goldValue;
	}

	public void setCurrentDiamondValue(int diamondValue) {

		support.firePropertyChange(this.agentName+";"+"diamond", this.currentDiamondValue, diamondValue);
		this.currentDiamondValue = diamondValue;
	}

	public void setCurrentNbMsgSent(int nbMsgSent) {

		support.firePropertyChange(this.agentName+";"+"sent", this.nbMsgSent, nbMsgSent);
		this.nbMsgSent = nbMsgSent;
	}

	public void setCurrentNbMsgReceived(int nbMsgReceived) {

		support.firePropertyChange(this.agentName+";"+"received", this.nbMsgReceived, nbMsgReceived);
		this.nbMsgReceived = nbMsgReceived;
	}

	public void setCurrentNbMsgSentAndDelivered(int nbMsgSentAndDelivered) {

		support.firePropertyChange(this.agentName+";"+"sentAndDelivered", this.nbMsgSentAndDelivered, nbMsgSentAndDelivered);
		this.nbMsgSentAndDelivered = nbMsgSentAndDelivered;
	}

	public String toString() {
		return this.agentName+" ;currentDiamondInBP: "+this.currentDiamondValue+";currentGoldInBP: "+this.currentGoldValue+"; nbSent: "+this.nbMsgSent+"; nbSent&Delivered: "+this.nbMsgSentAndDelivered+"; nbReceived: "+this.nbMsgReceived+"\n"; 
	}

	public String getAgentName() {
		return agentName;
	}

	public int getCurrentGoldValue() {
		return currentGoldValue;
	}

	public int getCurrentDiamondValue() {
		return currentDiamondValue;
	}

	public int getNbMsgSent() {
		return nbMsgSent;
	}

	public int getNbMsgSentAndDelivered() {
		return nbMsgSentAndDelivered;
	}

	public int getNbMsgReceived() {
		return nbMsgReceived;
	}

	public void setTreasureValue(Observation o,int value) {
		switch (o) {
		case DIAMOND:
			setCurrentDiamondValue(value);
			break;

		case GOLD:
			setCurrentGoldValue(value);
			break;
		default:
			System.err.println("This type of treasure does not exist");
			System.exit(0);
			break;
		}
	}

	public void add2TreasureValue(Observation o,int value) {
		switch (o) {
		case DIAMOND:
			add2CurrentDiamondValue(value);
			break;
		case GOLD:
			add2CurrentGoldValue(value);
			break;
		default:
			System.err.println("This type of treasure does not exist");
			System.exit(0);
			break;
		}
	}

	private void add2CurrentDiamondValue(int value) {

		support.firePropertyChange(this.agentName+";"+"diamond", this.currentDiamondValue, currentDiamondValue+value);
		this.currentDiamondValue = currentDiamondValue+value;

	}

	private void add2CurrentGoldValue(int value) {

		support.firePropertyChange(this.agentName+";"+"gold", this.currentGoldValue, currentGoldValue+value);
		this.currentGoldValue = currentGoldValue+value;

	}

	public int getBackPackUsedSpace(Observation myTreasureType) {
		int v=0;
		switch (myTreasureType) {
		case DIAMOND:
			v= getCurrentDiamondValue();
			break;
		case GOLD:
			v= getCurrentGoldValue();
			break;
		default:
			Debug.error("This type of treasure does not exist");
			break;
		}
		return v;
	}
	
	public boolean equals(Object e) {
		return this.agentName==((AgentObservableElement)e).getAgentName();
	}
}
