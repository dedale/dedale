package eu.su.mas.dedale.mas.agent.behaviours;

import java.util.List;

import dataStructures.tuple.Couple;
import debug.Debug;
import eu.su.mas.dedale.env.EntityCharacteristics;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.knowledge.AgentObservableElement;

import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

/**
 * This behaviour is automatically added to the Tanker agents.
 * PROTOCOL_TANKER="ProtocolTanker";
 * 
 * An agent with the appropriate type call the method EmptyMyBackPack(TankerName)
 * This method trigger a Request (TreasureType,Value) message
 * The current behaviour wait for it and store the result in the agent
 * 
 * @author hc
 *
 */
//TODO Create the Gui based on the the tanker backpack
public class ReceiveTreasureTankerBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = -6407154712384808742L;
	public static final String PROTOCOL_TANKER="ProtocolTanker";
	//private HashMap<Observation,Integer> backPack;
	private AgentObservableElement aoe;
	private EntityCharacteristics ec;

	public ReceiveTreasureTankerBehaviour(AbstractDedaleAgent a,AgentObservableElement backPack,EntityCharacteristics ec) {
		super(a);
		this.aoe=backPack;
		this.ec=ec;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void action() {

		//Filter the messages
		MessageTemplate template= 
				MessageTemplate.and(
						MessageTemplate.MatchProtocol(PROTOCOL_TANKER),
						MessageTemplate.MatchPerformative(ACLMessage.REQUEST)		
						);

		//I'm waiting for a message from a collector
		ACLMessage msg=this.myAgent.receive(template);
		//Couple<Observation,Integer> c=null;
		List<Couple<Observation, Integer>>c=null;
		if (msg!=null){
			//Debug.warning("Tanker agent - Message received: "+msg.toString());
			try {
				c=(List<Couple<Observation, Integer>>) msg.getContentObject();
			} catch (UnreadableException e) {
				Debug.error("Tanker receiving non Deserializable value");
				e.printStackTrace();
			}
			c.forEach(x -> {
				Integer i=this.ec.getBackPackCapacity(x.getLeft())-this.aoe.getBackPackUsedSpace((x.getLeft()));
				if (i>0){
					//	add the received value in the agentTanker backpack
					this.aoe.add2TreasureValue(x.getLeft(), x.getRight());
					//Debug.warning(c.getLeft()+" - There is now "+this.backPack.get(c.getLeft()) +" in the backPack");
				}else{
					this.aoe.add2TreasureValue(x.getLeft(), i);//the remaining is lost
				}
			});
			//Integer i=this.ec.getBackPackCapacity(c.getLeft())-this.aoe.getBackPackUsedSpace((c.getLeft()));
			
			ACLMessage resp=msg.createReply();
			resp.setPerformative(ACLMessage.AGREE);
			this.myAgent.send(resp);
		}else{
			block();
		}
	}
	

	@Override
	public boolean done() {
		return false;
	}

}
