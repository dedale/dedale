package eu.su.mas.dedale.mas.agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import agent.AbstractDeltaAgent;
import debug.Debug;
import eu.su.mas.dedale.env.EnvironmentType;
import eu.su.mas.dedale.env.GeneratorType;
import eu.su.mas.dedale.env.IEnvironment;
import eu.su.mas.dedale.env.gs.GsGeneratorType;
import eu.su.mas.dedale.env.gs.GsEnvironment;
import eu.su.mas.dedale.env.jme.JmeEnvironment;
import eu.su.mas.dedale.mas.agent.behaviours.RandomWalkBehaviour;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.KillListBehaviour;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.StartMyBehaviours;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.StartMyDelayedBehaviours;
import eu.su.mas.dedale.mas.agent.interactions.protocols.PdeployMe;
import eu.su.mas.dedale.mas.agent.interactions.protocols.Pterminate;
import eu.su.mas.dedale.princ.ConfigurationFile;
//import env.Environment;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
//import mas.agents.interactions.protocols.P_deployMe;
//import mas.agents.interactions.protocols.deployMe.R2_envManager;
//import mas.behaviours.RandomWalkBehaviour;
//import mas.behaviours.SayHello;
import jade.util.InputQueue;

/**
 * The gate-keeper ensure that 
 *  - there is a possibility to add an agent within the environment
 *  - that the agent is compatible with the types of environment
 *  - the agent receives a reference to the environment
 *  
 * @author hc
 *
 */
public class GateKeeperAgent extends AbstractDeltaAgent {

	private static final long serialVersionUID = 337962546911985502L;


	//TODO Ensure that there is only one gatekeeper  on a given container.
	//Environment ref that all the agents within the container should share.
	private IEnvironment env;

	
	
	protected void setup(){

		super.setup();

		//1) get the parameters given into the object[]. In the current case, the type of environment where the agents will evolve
		final Object[] args = getArguments();
		
		Assert.assertNotNull(args);

		//Assert.assertEquals(args.length, 8);
		switch ((EnvironmentType) args[0]) {
		case GS:
			env= new GsEnvironment();
			env.createEnvironment((GeneratorType) args[1],(String)args[2], (String)args[3],(boolean)args[4],(boolean)args[5],(boolean)args[6],(Integer[])args[7]);
			break;
		case JME:
			env = new JmeEnvironment();
			//env.CreateEnvironment((String)args[0], (String)args[1],(boolean)args[3],(Integer)args[4],(boolean)args[5],(boolean)args[6],(boolean)args[7]);
			env.createEnvironment((GeneratorType) args[1],(String)args[2], (String)args[3],(boolean)args[4],(boolean)args[5],(boolean)args[6],(Integer[])args[7]);
			break;	
		default:
			Debug.error("This type of environment is not yet supported");
			break;
		}


		//3) Add the behaviour(s) allowing the GK to add/remove agent from the env
		PdeployMe p= new PdeployMe();
		addBehaviour(p.new R2envManager(this,env));
		
		//4) Add the behaviour(s) to work with the Gui
		List<Behaviour> lb=new ArrayList<Behaviour>();
		lb.add(new KillListBehaviour(this,env));
		
		addBehaviour(new StartMyDelayedBehaviours(this,lb,2000));
		
		//5) Add the behaviour to deal with killing agents
		//this behaviour should be added with ability from deltas
		Pterminate p1= new Pterminate();
		addBehaviour(p1.new R1receiveTransferTerminate(this));
		
		System.out.println("the agent "+this.getLocalName()+ " is started");

	}


}
