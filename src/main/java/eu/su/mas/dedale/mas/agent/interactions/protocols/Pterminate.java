package eu.su.mas.dedale.mas.agent.interactions.protocols;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.Assert;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
@startuml
hide footbox
title Terminate protocol

activate User
User -> Gk :Req(terminate,A)
deactivate User
activate Gk
activate Gk
alt A is here
Gk -> A  :Req(terminate)
deactivate Gk
activate A #DarkSalmon
A -> A: DoDelete
destroy A
else A in another platform
Gk ->] :Req(Gki, terminate, A)
deactivate Gk
A<-] : Req(terminate)
activate A #DarkSalmon
A -> A: DoDelete
destroy A
end
@enduml
*/

/**
 * <pre>
 * This class is used to allow an agent to ask another one to terminate itself.
 * If the agent is not on the platform, the GK will contact its GK's neighbours and transfer the request
 * SimpleCases :
 *  GateKeeper --request(terminate) --> A
 *  or 
 * Someone -- request(terminate,A1) --> Gk
 * GK -- request(terminate) --> A1
 * 
 * </pre>
 * @author Cédric Herpson
 */


public class Pterminate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7091856959753507166L;
	public final static String PROTOCOLNAME="P_terminate";
	

	public class R1sendTerminate extends OneShotBehaviour {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2367660115030548924L;
		public final static String PROTOCOLNAME="P_terminate";

		private String agentToTerminateName;

		/**
		 * 
		 * @param agentToTerminateName localName of the agent
		 * @param agent this
		 */
		public R1sendTerminate(String agentToTerminateName, Agent agent) {
			super(agent);
			Objects.requireNonNull(agentToTerminateName,"The agent to terminate must have a name");
			//Assert.assertNotSame("the agent to terminate must have a name", "",agentToTerminateName);
			this.agentToTerminateName=agentToTerminateName;
		}

		@Override
		public void action() {

			//1) check that the agent is within the environment managed by the Gk.
			List<String> agentList=getAgentsList();
			//TODO Should use the getKnowledge instead
			//((AbstractDeltaAgent) this.myAgent).getKnowledge(abilityID);


			ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
			msg.setSender(this.myAgent.getAID());
			msg.setProtocol(PROTOCOLNAME);

			if (agentList.contains(agentToTerminateName)){
				//2) If yes, send the request to the agent
				msg.addReceiver(new AID(this.agentToTerminateName,AID.ISLOCALNAME));
				msg.setContent("terminate");
			} else {
				msg.setContent(this.agentToTerminateName);
				//TODO get the list of other GK
				//3) If no, transfer the request to other(s) Gks
				//3.1) get the list of other GK
				//for (String gkName :gkList){
				//		msg.addReceiver(new AID(gkName,AID.ISLOCALNAME));
				//}
			}			

			this.myAgent.send(msg);
		}

		/**
		 * 
		 * @return The list of the (local)names of the agents currently within the platform 
		 */
		private List<String> getAgentsList(){
			AMSAgentDescription [] agentsDescriptionCatalog = null;
			List<String> agentsNames= new ArrayList<String>();

			try {
				SearchConstraints c = new SearchConstraints();
				c.setMaxResults ( new Long(-1) );
				agentsDescriptionCatalog = AMSService.search(this.myAgent, new AMSAgentDescription (), c );
			}
			catch (Exception e) {
				System.out.println( "Problem searching AMS: " + e );
				e.printStackTrace();
			}

			for (int i=0; i<agentsDescriptionCatalog.length;i++){
				AID agentID = agentsDescriptionCatalog[i].getName();
				agentsNames.add(agentID.getLocalName());
			}
			return agentsNames;
		}

	}

	/**
	 * Always listening on the Gk
	 * @author Cédric Herpson
	 *
	 */
	public class R1receiveTransferTerminate extends SimpleBehaviour{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 8625442685513062401L;
		public R1receiveTransferTerminate(Agent agent) {
			super(agent);
		}

		public void action() {
			MessageTemplate template= MessageTemplate.and(
					MessageTemplate.MatchProtocol(PROTOCOLNAME),
					MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
			ACLMessage msg=this.myAgent.receive(template);

			if (msg!=null){
				this.myAgent.addBehaviour(new R1sendTerminate(msg.getContent(), this.myAgent));
			}else{
				block();
			}
		}
		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return false;
		}

	}
	
	/**
	 * Always listening on the agent to kill it if requested
	 * @author Cédric Herpson
	 *
	 */
	public class R2receiveTerminate extends SimpleBehaviour{
		
		
		private static final long serialVersionUID = 5867476930701510706L;
		private boolean done=false;

		public R2receiveTerminate(Agent agent) {
			super(agent);
		}

		public void action() {
			MessageTemplate template= MessageTemplate.and(
						MessageTemplate.MatchProtocol(PROTOCOLNAME),
						MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
			
			ACLMessage msg=this.myAgent.receive(template);
			//System.out.println("Value of the received msg"+msg);
			if (msg!=null){
				this.myAgent.doDelete();//This automatically call takeDown on the agent
				done=true;
			}else{
				block();
			}
		}
		
		@Override
		public boolean done() {
			// TODO Auto-generated method stub
			return done;
		}

	}


}
