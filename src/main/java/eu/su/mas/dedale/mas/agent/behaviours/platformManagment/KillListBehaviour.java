package eu.su.mas.dedale.mas.agent.behaviours.platformManagment;

import debug.Debug;
import eu.su.mas.dedale.env.IEnvironment;
import eu.su.mas.dedale.env.gs.GsEnvironment;
import eu.su.mas.dedale.mas.agent.interactions.protocols.Pterminate;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.util.InputQueue;

/**
 * When the user select a KillButton, it trigger a method in the gui that update a list.
 * This list calls the KillListBehaviour below (already running) that adds the behaviour associated with the P-terminate protocol 
 * @author hc
 *
 */
public class KillListBehaviour extends SimpleBehaviour {


	private static final long serialVersionUID = 7399388265680005947L;
	//private boolean initDone;
	private IEnvironment env;
	private boolean associated;

	/**
	 * Behaviour used to terminate an agent selected by the user through the GUI
	 * 
	 * @param a agent
	 * @param e environment the terminated target is living in
	 */
	public KillListBehaviour(Agent a,IEnvironment e) {
		super(a);
		env=e;
		associated=false;
	}

	@Override
	public void action() {
		
		if (!associated) {
			((GsEnvironment)this.env).getGuiController().linkBehaviourToKlist(this);
			associated=true;
		}
		
		String s=((GsEnvironment)this.env).getAgentToKill();
		if (s!=null) {
			//System.out.println("KillSwitchBehaviour trigered with : "+s);
			Pterminate p1= new Pterminate();
			this.myAgent.addBehaviour(p1.new R1sendTerminate(s, this.myAgent));
		}else {
			//System.out.println("KillSwitchBehaviour initiated, blocked and waiting for a target");
			block();
		}

	}

	@Override
	public boolean done() {
		return false;
	}

}
