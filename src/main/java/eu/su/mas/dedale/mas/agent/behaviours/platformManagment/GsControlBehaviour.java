package eu.su.mas.dedale.mas.agent.behaviours.platformManagment;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import dataStructures.tuple.Couple;
import debug.Debug;
import eu.su.mas.dedale.env.IEnvironment;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.GsEnvironment;
import eu.su.mas.dedale.gui.MyController;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

/**
 * Behaviour used by the GateKeeper when a user controlled agent is deployed in the environment.
 * The "N"(ext) key is used to iterate over the possible destinations
 * The "O"(k) key is used to validate to selected path 
 * @author hc
 *
 */

public class GsControlBehaviour extends TickerBehaviour implements PropertyChangeListener{
	// Should not be a ticker. An outside listener could notify the behaviour see pending issues
	
	private IEnvironment env;
	/**
	 * 
	 */
	private static final long serialVersionUID = -4286625518389029016L;

	private boolean keyUpdated;
	private boolean initDone;
	private String keyPressed;

	public GsControlBehaviour(Agent a, long period, IEnvironment env) {
		super(a, period);
		this.env=env;
		this.keyUpdated=false;
		this.initDone=false;
		this.keyPressed=null;
		Debug.info(this.myAgent.getLocalName()+" - Behaviour : "+this.getBehaviourName()+" added");
	}


	@Override
	protected void onTick() {
		boolean move=false;
		Location nextPosition=null;
		if (!initDone) {
			if (this.env!=null) {
				((GsEnvironment)this.env).getGuiController().getKeyObservable().addPropertyChangeListener(this);
				this.initDone=true;	
				Debug.info("Initialisation of user controlled agent "+this.myAgent.getLocalName()+ "done");
			}else {
				Debug.error(this.getBehaviourName() + "- Env is null");
			}
		}
		if (keyUpdated) {
			System.out.println("The key is updated");
			System.out.println("Its time to do the job - waiting for order");
			Location currentLocation=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
			switch (keyPressed) {
			case "O"://choosen, we move
				nextPosition=((GsEnvironment)this.env).getUserChoosenPath(this.myAgent.getLocalName());
				move=true;
				break;
			case "N"://the current proposed path is not satisfactory, the user switch to next
				List<Couple<Location, List<Couple<Observation, String>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();
				System.out.println("State of the observations : "+lobs);
				((GsEnvironment)this.env).updateUserChoosenPath(this.myAgent.getLocalName());
				break;
			}
			keyUpdated=false;
		}else {
			//System.out.println("The key has not been updated since last time"); 
		}
		if (move) {
			Debug.info("On the move");
			move=false;
			((AbstractDedaleAgent)this.myAgent).moveTo(nextPosition);
		}
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		keyPressed=(String) evt.getNewValue();
		switch (keyPressed) {
		case "O"://path choosen doMove
			System.out.println("O is pressed and the agent knowns it");
			keyUpdated=true;

			break;
		case "N"://next path update the gui
			System.out.println("N is pressed and the agent knowns it");
			keyUpdated=true;
			break;
		default:
			Debug.warning(this.getClass().getName()+" - This key is not used");
			break;
		}
		
		
	}

}
