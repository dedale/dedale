package eu.su.mas.dedale.mas.agent.behaviours;

import java.util.Objects;

import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.AMSService;

import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;

/**
 * Should be made available in the knowledge base
 * @author User
 *
 */
public class AgentListFromAMSBehaviour extends OneShotBehaviour {

	private static final long serialVersionUID = 8238681040865273539L;

	@Override
	public void action() {

		AMSAgentDescription [] agentsDescriptionCatalog = null;
        
        try {
            SearchConstraints c = new SearchConstraints();
            c.setMaxResults ( Long.valueOf(-1) );
            agentsDescriptionCatalog = AMSService.search(this.myAgent, new AMSAgentDescription (), c );
        }
        catch (Exception e) {
            System.out.println( "Problem searching AMS: " + e );
            e.printStackTrace();
		}
        
        Objects.requireNonNull(agentsDescriptionCatalog);
		for (int i=0; i<agentsDescriptionCatalog.length;i++){
			AID agentID = agentsDescriptionCatalog[i].getName();
			System.out.println(agentID.getLocalName());
		}
		
		

	}


}
