package eu.su.mas.dedale.mas;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.ContainerController;
import jade.wrapper.ControllerException;
//import knowledge.LocationK;

import org.junit.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

import agent.AbstractDeltaAgent;
import dataStructures.tuple.Couple;
import debug.Debug;

import eu.su.mas.dedale.env.ElementType;
import eu.su.mas.dedale.env.EntityCharacteristics;
import eu.su.mas.dedale.env.EntityType;
import eu.su.mas.dedale.env.IEnvironment;
import eu.su.mas.dedale.env.Location;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.env.gs.GsEnvironment;
import eu.su.mas.dedale.env.gs.GsLocation;
import eu.su.mas.dedale.env.mapElements.LockElement.LockType;
import eu.su.mas.dedale.mas.agent.behaviours.ReceiveTreasureTankerBehaviour;
import eu.su.mas.dedale.mas.agent.behaviours.platformManagment.GsControlBehaviour;
import eu.su.mas.dedale.mas.agent.interactions.protocols.PdeployMe;
import eu.su.mas.dedale.mas.agent.interactions.protocols.Pterminate;
import eu.su.mas.dedale.mas.agent.knowledge.AgentObservableElement;

import eu.su.mas.dedale.princ.ConfigurationFile;
import eu.su.mas.dedale.tools.EntityCharacteristicFromJSON;


/**
 * <pre>
 * Any agent willing to interact with a the DEDALE environment must extend this class it offers :
 * <ul>
 *  <li> the API to move and act in the env, either it is distributed or not</li>
 *  <li> the management of the communication reach</li>
 *  <li> the deployment of the agent</li>
 *  </ul>
 * </pre>
 * @author Cédric Herpson.
 *
 */
public class AbstractDedaleAgent extends AbstractDeltaAgent {

	private static final long serialVersionUID = -7435630598237152610L;

	private EntityCharacteristics ec;

	/**
	 * The key is the Observation used as an ObservationType
	 * The value is the Observation object. Currently two elements of the same class, but not in the future 
	 */
	//private HashMap<Observation,Integer> myBackPack;

	/**
	 * All the agent's dynamic elements that we might want to share or log (backpack,..).
	 */
	private AgentObservableElement aoe;



	/**
	 * A private ref to the environment the agent is moving into, should never be directly accessible to the user
	 * It is initiated by the gatekeeper when the agent is deployed 
	 * It should be obtained 
	 */
	private IEnvironment realEnv;

	/**
	 * Used to get the ref to the environment when asking the GK 
	 */
	private String environmentName;


	private String gateKeeperName;


	protected Random r;
	
	public AbstractDedaleAgent(){
		super();
		this.r = new Random();
		//this.aoe=new AgentObservableElement(agentName)
	}

	/**
	 * 
	 * @param e the type of agent to create
	 * @param ec its characteristics
	 * @param environmentName name of the environment to deploy the agent
	 */
	public AbstractDedaleAgent(EntityType e,EntityCharacteristics ec, String environmentName){
		super();	
		this.r=new Random();
		Assert.assertNotNull(e);
		Assert.assertNotNull(ec);
		Assert.assertNotNull(environmentName);
		this.ec=ec;
		//this.myBackPack=new HashMap<Observation, Integer>();
		this.environmentName=environmentName;
		this.aoe=new AgentObservableElement(ec.getAgentName());

		//see AAgent (EnvironmentManager) //registerO2AInterface(EnvironmentManager.class,this);
	}


	/**
	 * 
	 * @return The agent's current position, null if the agent is not in the environment
	 */
	public Location getCurrentPosition(){
		if (realEnv==null){
			Debug.warning("The agent is currently not deployed in any environment, you should not call getCurrentPosition()");
			return null;
		}		
		return this.realEnv.getCurrentPosition(this.getLocalName());
	}


	/**
	 *  The available observations from the agent's current position.
	 * @return A list of observed position (PositionID), and for each one is associated its list of observations under the form (ObservationType, Value))
	 * . Null if there is a malfunction<p>
	 * Example : {<p> Position1; [(Observation1,Value1);(Observation2,Value2)],
	 * <p>Position2; [(Observation1,Value1);(Observation2,Value2)],<p>
	 * ..}  
	 * 
	 * @see Observation for the list of Observation components
	 */
	public synchronized List<Couple<Location, List<Couple<Observation,String>>>> observe(){
		return this.realEnv.observe(this.getCurrentPosition(),this.getLocalName());
	}


	/**
	 * This method should be the last method called in your behaviour.<p>
	 * The agent can die if he moves too carelessly.
	 * @param myDestination the targeted nodeId
	 * @return true if the move is legit and triggered, false otherwise
	 */
	public synchronized boolean moveTo(Location myDestination){
		int consequence=this.realEnv.moveTo(this.getLocalName(),this.ec, myDestination);
		if(consequence==1){
			return true;
		}else{
			if (consequence==-1){
				//if an event in the env killed the agent, destroy the agent
				this.doDelete();
			}
			//Otherwise, just a move refused
			return false;
		}
	}


	/**
	 * The amount of wealth that the agent was able to pick. 
	 * @return 0 if there is no treasure at this place, or if the agent cannot grab it (
	 * backPack full, not authorized,...)
	 */
	public synchronized int pick(){
		//TODO update the agent BackPack and decide on the quantity according to its capability
		int pickedQuantity=0;
		Observation pickedType=ec.getMyTreasureType();
		switch (ec.getMyTreasureType()) {
		case DIAMOND:
			pickedQuantity= this.realEnv.pick(this.getLocalName(),this.getCurrentPosition(),
					ElementType.DIAMOND,
					ec.getDiamondCapacity()-this.aoe.getCurrentDiamondValue());//-this.myBackPack.get(ec.getMyTreasureType()));
			break;
		case GOLD:
			pickedQuantity=this.realEnv.pick(this.getLocalName(),this.getCurrentPosition(),
					ElementType.GOLD,
					ec.getGoldCapacity()-this.aoe.getCurrentGoldValue());//this.myBackPack.get(ec.getMyTreasureType()));
			break;
		case ANY_TREASURE:
			//There can be only one type of safe on a given place.
			int pickedQuantity1= this.realEnv.pick(this.getLocalName(),this.getCurrentPosition(),
					ElementType.DIAMOND,
					ec.getDiamondCapacity()-this.aoe.getCurrentDiamondValue());
			int pickedQuantity2=this.realEnv.pick(this.getLocalName(),this.getCurrentPosition(),
					ElementType.GOLD,
					ec.getGoldCapacity()-this.aoe.getCurrentGoldValue());
			if (pickedQuantity1>0 && pickedQuantity2>0) {
				Debug.error("This situation should not currently occur as only one treasure should be present on a given node.");
			}else {
				// The type of the agent is updated to the one of the treasure it collected
				if(pickedQuantity1>pickedQuantity2) {
					pickedType=Observation.DIAMOND;
					pickedQuantity=pickedQuantity1;
					this.ec.setMyTreasureType(Observation.DIAMOND);

				}else{
					pickedType=Observation.GOLD;
					pickedQuantity=pickedQuantity2;						
					this.ec.setMyTreasureType(Observation.GOLD);
				}
			}
			break;
		default:
			Debug.warning("The agent's type does not allow him to pick anything in this configuration of the project");
			pickedQuantity= 0;
			break;
		}

		if (pickedQuantity>0){
			//update the backpack
			//this.myBackPack.put(getMyTreasureType(),this.myBackPack.get(getMyTreasureType())+pickedQuantity);
			this.aoe.add2TreasureValue(pickedType, pickedQuantity);
		}
		return pickedQuantity;
	}

	/**
	 * This methods automatically aggregates the surrounding connected agents expertise 
	 * @param o The type of treasure to open (Observation.Gold, Observation.Diamond, Observation.Any).
	 * @return true is the lock is open, false otherwise
	 */
	public synchronized boolean openLock(Observation o){
		boolean result=false;
		switch (o){
		case DIAMOND: 
			result=this.realEnv.openLock(this.getLocalName(),this.getCurrentPosition(),ElementType.DIAMOND);
			break;
		case GOLD :
			result=this.realEnv.openLock(this.getLocalName(),this.getCurrentPosition(),ElementType.GOLD);
			break;	
		case ANY_TREASURE:
			result= this.realEnv.openLock(this.getLocalName(),this.getCurrentPosition(),ElementType.GOLD) || this.realEnv.openLock(this.getLocalName(),this.getCurrentPosition(),ElementType.DIAMOND);
			break;
		default :
			Debug.warning(this.getClass().getName()+ "- This type of Observation does not correspond to a treasure type: "+o);
			break;
		}
		return result;		
	}

	/**
	 * Close all the safes present on the current position.
	 * @return true if at least one chest with a lock was on the agent position (all chest on the position are now locked)
	 */
	public synchronized boolean closeLock() {
		return this.realEnv.closeLock(this.getCurrentPosition());
	}
	/**
	 * The available carrying capacity of the agent according to its type.
	 * @return As an agent can carry several type of resources, its a list of Couple{@literal <Observation,Value>}
	 * 
	 */
	public List<Couple<Observation,Integer>> getBackPackFreeSpace(){
		List<Couple<Observation,Integer>> backPackFreeSpace = new ArrayList<Couple<Observation,Integer>>();
		switch (ec.getMyTreasureType()) {
		case DIAMOND:
			//return ec.getDiamondCapacity()-this.myBackPack.get(ec.getMyTreasureType());
			//return ec.getDiamondCapacity()-this.aoe.getCurrentDiamondValue();
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.DIAMOND,ec.getDiamondCapacity()-this.aoe.getCurrentDiamondValue()));
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.GOLD,0));
			break;
		case GOLD:
			//return ec.getGoldCapacity()-this.myBackPack.get(ec.getMyTreasureType());
			//return ec.getGoldCapacity()-this.aoe.getCurrentGoldValue();
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.GOLD,ec.getGoldCapacity()-this.aoe.getCurrentGoldValue()));
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.DIAMOND,0));
			break;
		case ANY_TREASURE:
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.GOLD,ec.getGoldCapacity()-this.aoe.getCurrentGoldValue()));
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.DIAMOND,ec.getDiamondCapacity()-this.aoe.getCurrentDiamondValue()));
			break;
		case NO_TREASURE :
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.DIAMOND,0));
			backPackFreeSpace.add(new Couple<Observation, Integer>(Observation.GOLD,0));
			break;
		default:
			Debug.error("This type is not a treasureType: "+ec.getMyTreasureType());
			return null;
		}
		return backPackFreeSpace;

	}

	/**
	 * The type of treasure that the agent is able to pick
	 * @return The type of treasure that the agent is able to pick
	 */
	public Observation getMyTreasureType(){
		return this.ec.getMyTreasureType();
	}

	/**
	 * The expertise of the agent
	 * @return The expertise of the agent as a set of Couple(Observation,Integer)
	 */
	public Set<Couple<Observation,Integer>> getMyExpertise(){
		Set<Couple<Observation, Integer>> s= new HashSet<Couple<Observation, Integer>>();
		Iterator<Couple<LockType, Integer>> iter=this.ec.getExpertise().iterator();
		while (iter.hasNext()){
			Couple<LockType, Integer> c=iter.next();
			switch (c.getLeft()) {
			case LOCKPICKING:
				s.add(new Couple<Observation, Integer>(Observation.LOCKPICKING, c.getRight()));
				break;
			case STRENGTH :
				s.add(new Couple<Observation, Integer>(Observation.STRENGH, c.getRight()));
				break;
			default:
				Debug.error(this.getClass().getName()+" - this expertise is not yet managed");
				break;
			}
		}
		return s;
	}
	/**
	 * Allow the agent to transfer its backpack within a Tanker agent if there is one in the vicinity
	 * 
	 * @param agentSiloName name of the Tanker agent to receive the backpack's content
	 * @return true if you're agent was allowed to realize the transfer, false otherwise 
	 * 
	 */
	public synchronized boolean emptyMyBackPack(String agentSiloName){

		//I cannot be sure that the targetAgent is a silo before sending the message.
		//Protocol SendTreasure:
		// A -(type,quantity)-> B
		//Message arrive only if reacheable.
		// if B ok -(done)->A
		// if B nok -(refused)->A
		//A return false;

		//chose to do it in the critical section with sendMessage, but will block the agent
		//or asynchronously

		//critical section is preferred to check the agentSiloName type and ensure the boolean answer. Otherwise I cannot give a boolean answer to the agent.
		//At the same time it is normal to block the agent during the realisation of a task.
		//conception decision to explain in the documentation

		if (ec.getMyTreasureType()!=Observation.NO_TREASURE && this.realEnv.isReachable(this.getLocalName(),agentSiloName,this.ec.getCommunicationReach())){
			//this.addAbility(ability, abilityID, role, behavioursParameters, knowledge);
			//this.addBehaviour(new SendTreasure(this,agentSiloName,this.myTreasureType,this.myBackPack.get(this.myTreasureType)));

			ACLMessage msg= new ACLMessage(ACLMessage.REQUEST);
			msg.setProtocol(ReceiveTreasureTankerBehaviour.PROTOCOL_TANKER);
			msg.setSender(this.getAID());
			msg.addReceiver(new AID(agentSiloName,AID.ISLOCALNAME));

			//Couple<Observation, Integer> c= new Couple<Observation, Integer>(ec.getMyTreasureType(), this.myBackPack.get(ec.getMyTreasureType()));
			Couple<Observation, Integer> cgold= new Couple<Observation, Integer>(Observation.GOLD, this.aoe.getBackPackUsedSpace(Observation.GOLD));
			Couple<Observation, Integer> cdiam= new Couple<Observation, Integer>(Observation.DIAMOND, this.aoe.getBackPackUsedSpace(Observation.DIAMOND));
			List<Couple<Observation, Integer>>c=new ArrayList<Couple<Observation,Integer>>();
			c.add(cgold);
			c.add(cdiam);
			try {
				msg.setContentObject((Serializable)c);
			} catch (IOException e) {
				Debug.error("EmptyMyBackPack - non serializable");
				e.printStackTrace();
			}
			sendMessage(msg);

			//Filter the messages
			MessageTemplate template= 
					MessageTemplate.and(
							MessageTemplate.MatchSender(new AID(agentSiloName, AID.ISLOCALNAME)),
							MessageTemplate.and(
									MessageTemplate.MatchProtocol(ReceiveTreasureTankerBehaviour.PROTOCOL_TANKER),
									MessageTemplate.MatchPerformative(ACLMessage.AGREE)		
									)
							);

			//I'm waiting for a message indicating that the silo received the value. If not, then the receiver is not a silo
			ACLMessage msg2=this.blockingReceive(template,4000);

			if (msg2!=null){
				//clear mybackpack
				//this.myBackPack.put(ec.getMyTreasureType(), 0);
				//this.aoe.setTreasureValue(ec.getMyTreasureType(), 0);
				this.aoe.setTreasureValue(Observation.GOLD,0);
				this.aoe.setTreasureValue(Observation.DIAMOND, 0);
				//temp gui because the listener is not working
				((GsEnvironment)this.realEnv).updateAgentsStatsWithinGui();
				//((gsEnvironment)this.realEnv).UpdateGuiTreasure(this.getLocalName(),-c.getRight());
				//((gsEnvironment)this.realEnv).UpdateGuiTreasure(agentSiloName,c.getRight());
				//end temp
				return true;
			}else{
				return false;
			}			
		}else{
			return false;
		}
	}

	/**
	 * Throw a grenade to a given location.
	 * @param locationId the Id of the room the entity is aiming at
	 * @return true if done, false otherwise (the location as to far,..) 
	 * The consequences of this action, if any, will be seen in the environment
	 */

	public boolean throwGrenade(Location locationId){
		return this.realEnv.throwGrenade(this.getLocalName(), locationId);
	}

	/**
	 * Drop the treasure on the current position, if allowed (depending of the agent's type) and elements already on the current location (one type of treasure per node)
	 */
	public void dropOff(){
		boolean b=false;
		if(ec.getMyEntityType()==EntityType.WUMPUS){//ec.getMyEntityType()==EntityType.WUMPUS_MOVER || 
			if (ec.getMyTreasureType()==Observation.ANY_TREASURE) {
				if (this.aoe.getCurrentGoldValue()>0 && this.aoe.getCurrentDiamondValue()>0) {
					//Random r=new Random();
					if(r.nextBoolean()) {
						b=this.realEnv.dropOff(this.getCurrentPosition(),ElementType.DIAMOND,this.aoe.getBackPackUsedSpace(Observation.DIAMOND));
						if (b) this.aoe.setTreasureValue(Observation.DIAMOND,0);
					}else {
						b=this.realEnv.dropOff(this.getCurrentPosition(),ElementType.GOLD,this.aoe.getBackPackUsedSpace(Observation.GOLD));
						if (b) this.aoe.setTreasureValue(Observation.GOLD,0);
					}
				}else {
					if (this.aoe.getCurrentGoldValue()>0) {
						b=this.realEnv.dropOff(this.getCurrentPosition(),ElementType.GOLD,this.aoe.getBackPackUsedSpace(Observation.GOLD));
						if (b) this.aoe.setTreasureValue(Observation.GOLD,0);
					}else {
						if (this.aoe.getCurrentDiamondValue()>0) {
							b=this.realEnv.dropOff(this.getCurrentPosition(),ElementType.DIAMOND,this.aoe.getBackPackUsedSpace(Observation.DIAMOND));
							if (b) this.aoe.setTreasureValue(Observation.DIAMOND,0);
						}
					}
				}
			}else {
				switch (ec.getMyTreasureType()) {
				case DIAMOND: 
					//this.realEnv.dropOff(this.getCurrentPosition(),ElementType.DIAMOND,this.myBackPack.get(ec.getMyTreasureType()));
					//this.myBackPack.put(getMyTreasureType(),0);
					b= this.realEnv.dropOff(this.getCurrentPosition(),ElementType.DIAMOND,this.aoe.getBackPackUsedSpace(Observation.DIAMOND));
					if (b) this.aoe.setTreasureValue(getMyTreasureType(),0);
					break;
				case GOLD:
					//this.realEnv.dropOff(this.getCurrentPosition(),ElementType.GOLD,this.myBackPack.get(ec.getMyTreasureType()));
					b= this.realEnv.dropOff(this.getCurrentPosition(),ElementType.GOLD,this.aoe.getBackPackUsedSpace(Observation.GOLD));
					if (b) this.aoe.setTreasureValue(getMyTreasureType(),0);
					break;
				default:
					Debug.warning("The agent's treasure type does not allow him to use dropOff()");
					break;
				}
			}
		}else{
			Debug.warning("The agent's type does not allow him to use the dropOff method");
		}
		if (b)
			//temp gui because the listener is not working
			((GsEnvironment)this.realEnv).updateAgentsStatsWithinGui();
	}

	/**
	 * This method MUST be used instead of the final method JADE.core.Agent.send()  in order for the platform 
	 * to control the reach of the communications. To receive the message, use the original JADE.core.Agent.receive() method. 
	 * @param msg the message to send
	 */
	//TODO wait for JadeIterator Update then clear the receiverNumber
	public void sendMessage(ACLMessage msg){
		Objects.requireNonNull(msg.getSender(), "The sender must have been defined");
		//filter reachable agents
		Iterator<AID> iter=msg.getAllReceiver();
		int receiverNumber=0;
		int nbPotentialContact=0;
		String senderLocalName =msg.getSender().getLocalName();
		while (iter.hasNext()){
			AID receiverAID= iter.next();
			receiverNumber++;
			nbPotentialContact++;
			if (!this.realEnv.isReachable(senderLocalName,receiverAID.getLocalName(),ec.getCommunicationReach())){
				iter.remove();
				receiverNumber--;
			}
		}

		if (receiverNumber>0){
			super.send(msg);
			//((gsEnvironment)this.realEnv).updateNodeInterfaceComRendering(receiverNumber);
			((GsEnvironment)this.realEnv).updateCommunicationStats(nbPotentialContact, receiverNumber);
		}else{
			//System.out.println(msg.getSender().getLocalName()+"-- No agent within reach --");
			((GsEnvironment)this.realEnv).updateCommunicationStats(nbPotentialContact, receiverNumber);
		}
	}

	/**
	 * Used to get a ref of this environment from the genericSharableObject table to realEnv, as well as the agents characteristics obtained through the configurationFile
	 * This methods is silently called by the agent when discussing with the gateKeeper.
	 * The end-user should NOT call it.
	 * */
	public void deployMe() {
		this.realEnv=(IEnvironment)this.getSharableObject(this.environmentName);

		Assert.assertNotNull(this.realEnv);
		this.realEnv.deployEntity(this.getLocalName(), this.ec,this.ec.getInitialLocation());
		//for the gui. be careful, only work for gs, a switch is necessary otherwise
		this.aoe.addPropertyChangeListener(((GsEnvironment)this.realEnv).getOoa());
		//this.ec=(EntityCaracteristics)this.getSharableObject(this.getLocalName());//assuming that the agent's name is unique
		if (ec.getMyEntityType()==EntityType.HUMAN) {
			addBehaviour(new GsControlBehaviour(this,300,this.realEnv));
		}
	}

	/**
	 * Search and load the characteristics of one agent.
	 * @param agentName localname of the agent
	 * @param instanceConfigurationEntitiesFullPath file describing the agent characteristics
	 * @return The entity characteristics as an object tabular
	 */
	public static Object[] loadEntityCaracteristics(String agentName, String instanceConfigurationEntitiesFullPath) {
		EntityCharacteristics ec;
		//explore the file, find the line for agent agentName
		Objects.requireNonNull(instanceConfigurationEntitiesFullPath, "The configuration file for the entity " +agentName+" is not given while you require to create it");
		if (instanceConfigurationEntitiesFullPath.endsWith(".json")){
			ec = loadEntityCharacteristicsFromJson(agentName, instanceConfigurationEntitiesFullPath);
		}else {
			ec = loadEntityCharacteristicsFromTxt(agentName, instanceConfigurationEntitiesFullPath);	
		}
		Object[] result={ec,ConfigurationFile.defaultGatekeeperName};
		return result;
	}

	/**
	 * <pre>
	 * Search and load the characteristics of one agent. 
	 * Original version, load from a txt file with compressed informations (agentName:x:y:z:t:..)
	 * </pre>
	 * @param agentName
	 * @param instanceConfigurationEntitiesFullPath
	 * @return the agent characteristics
	 */
	@Deprecated
	private static EntityCharacteristics loadEntityCharacteristicsFromTxt(String agentName,String instanceConfigurationEntitiesFullPath) {
		BufferedReader filereader=null;
		try {
			filereader = Files.newBufferedReader(Paths.get(instanceConfigurationEntitiesFullPath),StandardCharsets.UTF_8);
		} catch (IOException e1) {
			Debug.error("The configuration file for the entities does not exist");
			e1.printStackTrace();
		}

		EntityCharacteristics ec=null;
		boolean found=false;
		Objects.requireNonNull(filereader);
		try {	
			String line = filereader.readLine();
			String[]l=line.split(":");
			Debug.info("Loading entity "+ agentName +" for environment "+ l[1]+" from file: "+instanceConfigurationEntitiesFullPath);
			while (!found && filereader!=null && (line = filereader.readLine()) != null) {
				l=line.split(":");
				EntityType et=null;
				if (l[1].equals(agentName)){
					switch (l[0]) {
					case "wumpus":
						et=EntityType.WUMPUS;
						//deployWumpus(l[1],l[2],Integer.parseInt(l[3]),Integer.parseInt(l[4]));
						break;
					case "agentExplo":	
						et=EntityType.AGENT_EXPLORER;
						//deployAgentFromConfig(l[1], l[2],EntityType.AGENT_EXPLORER, Integer.parseInt(l[3]),Integer.parseInt(l[4]));
						break;	
					case "agentCollect":	
						et=EntityType.AGENT_COLLECTOR;
						//deployAgentFromConfig(l[1], l[2],EntityType.AGENT_COLLECTOR, Integer.parseInt(l[3]),Integer.parseInt(l[4]));
						break;	
					case "agentTanker":	
						et=EntityType.AGENT_TANKER;
						//deployAgentFromConfig(l[1], l[2],EntityType.AGENT_TANKER, Integer.parseInt(l[3]),Integer.parseInt(l[4]));
						break;	
					case "HumanControlled":
						et=EntityType.HUMAN;
						break;
					default :
						Debug.error("This Agent type does not currently exist"+l[0]);
					}
					//an expertise in lockpicking should be given
					//TODO move it in EntityCharacteristics and just give integer parameters
					Set<Couple<LockType,Integer>> expertise=new HashSet<Couple<LockType,Integer>>();
					expertise.add(new Couple<LockType, Integer>(LockType.LOCKPICKING, Integer.parseInt(l[7])));
					expertise.add(new Couple<LockType, Integer>(LockType.STRENGTH, Integer.parseInt(l[8])));

					//case where no location is given
					if (l[3].equalsIgnoreCase("free")){
						ec=new EntityCharacteristics(agentName,et,Integer.parseInt(l[5]),Integer.parseInt(l[4]),Integer.parseInt(l[2]),null,Integer.parseInt(l[6]),expertise);
					}else{
						ec=new EntityCharacteristics(agentName,et,Integer.parseInt(l[5]),Integer.parseInt(l[4]),Integer.parseInt(l[2]),new GsLocation(l[3]),Integer.parseInt(l[6]),expertise);	
					}
					found=true;
				}
			}
		} catch (IOException|ArrayIndexOutOfBoundsException e) {
			Debug.error("configuration file describing entity "+agentName+ "is not reachable or malformed: "+instanceConfigurationEntitiesFullPath);
			Debug.error("Check the path and the file format (See :https://dedale.gitlab.io/page/tutorial/deployagents/)");
			e.printStackTrace();
		}

		if (!found){
			Debug.error("loadEntityCaracteristics - The agent "+agentName +" declared in Principal.java was not found in the configuration file: "+instanceConfigurationEntitiesFullPath+".\n Its mandatory to give its caracteristics. See https://dedale.gitlab.io/page/tutorial/deployAgents/");
		}
		return ec;
	}

	/**
	 * Search and load the characteristics of one agent from a Json file
	 * @param agentName
	 * @param instanceConfigurationEntitiesFullPath
	 * @return the agent characteristics
	 */
	private static EntityCharacteristics loadEntityCharacteristicsFromJson(String agentName,String instanceConfigurationEntitiesFullPath) {
		EntityCharacteristics ec=null;
		List<EntityCharacteristicFromJSON> listEntitiesCharacs =null;

		Debug.info("Loading entity "+ agentName +" from entities' file: "+instanceConfigurationEntitiesFullPath);
		//JSON processing
		ObjectMapper mapper = new ObjectMapper();

		try {
			// JSON file to Java object
			listEntitiesCharacs = Arrays.asList(mapper.readValue(new File(instanceConfigurationEntitiesFullPath), EntityCharacteristicFromJSON[].class));        	
		} catch (IOException e) {
			Debug.error("JSON configuration file describing entity "+agentName+ " is not reachable or malformed: \n"+instanceConfigurationEntitiesFullPath);
			Debug.error("Check the path and the file format (See :https://dedale.gitlab.io/page/tutorial/deployagents/)");
			e.printStackTrace();
		}
		Objects.requireNonNull(listEntitiesCharacs);
		// get the searched agent, if present in the file	
		Optional<EntityCharacteristicFromJSON> res= listEntitiesCharacs.stream().filter(x ->x.getAgentName().equalsIgnoreCase(agentName)).findFirst();

		if (res.isPresent()) {
			//setting the real EntityCharacteristic
			EntityCharacteristicFromJSON temp=res.get();
			EntityType et=null;
			switch (temp.getAgentType()){
			case "wumpus":
				et=EntityType.WUMPUS;
				break;
			case "agentExplo":	
				et=EntityType.AGENT_EXPLORER;

				break;	
			case "agentCollect":	
				et=EntityType.AGENT_COLLECTOR;

				break;	
			case "agentTanker":	
				et=EntityType.AGENT_TANKER;

				break;	
			case "HumanControlled":
				et=EntityType.HUMAN;
				break;
			default :
				Debug.error("This Agent type does not currently exist"+temp);
			}
			//an expertise in lockpicking should be given
			//TODO move it in EntityCharacteristics and just give integer parameters
			Set<Couple<LockType,Integer>> expertise=new HashSet<Couple<LockType,Integer>>();
			expertise.add(new Couple<LockType, Integer>(LockType.LOCKPICKING, temp.getLockPickingExpertise()));
			expertise.add(new Couple<LockType, Integer>(LockType.STRENGTH, temp.getStrengthExpertise()));

			//case where no location is given
			if (temp.getInitialLocation().getLocationId().equals("free")){
				ec=new EntityCharacteristics(agentName,et,temp.getBackPackCapacityDiamond(),temp.getBackPackCapacityGold(),temp.getCommunicationRange(),null,temp.getDetectionRadius(),expertise);
			}else{
				ec=new EntityCharacteristics(agentName,et,temp.getBackPackCapacityDiamond(),temp.getBackPackCapacityGold(),temp.getCommunicationRange(),temp.getInitialLocation(),temp.getDetectionRadius(),expertise);
			}
		} else {         
			Debug.error("loadEntityCharacteristics  (JSON) - The agent "+agentName +"declared in Principal.java was not found in the configuration file: "+instanceConfigurationEntitiesFullPath+".\n Its mandatory to give its caracteristics in this file. See https://dedale.gitlab.io/page/tutorial/deployAgents/");
		}
		return ec;
	}



	//	/********************************
	//	 * Visible to the project only
	//	 *******************************/
	//	/**
	//	 * Used by the Knowledge components
	//	 * @return the backpack
	//	 */
	//	protected HashMap<Observation,Integer> getBackPack(){
	//		return this.myBackPack;
	//	}

	/******************
	 * Agent Creation
	 ******************
	 */

	/**
	 * This method is automatically called when a Dedale agent is created.
	 * It initialize its internal variables and add the mandatory abilities of any dedale's agent : 
	 * <ul>
	 * <li> To deploy itself within the environment and let its internal treasure state be observable :DeployMe</li>
	 * <li> To received treasures if the agent's type is "tanker"</li>
	 * <li> To be called back home when the network migration is allowed</li>
	 * <li> To be killed from and by Home when the network migration is allowed</li>
	 * </ul> 
	 */
	protected void setup(){	
		super.setup();

		final Object[] args = getArguments();
		Assert.assertNotNull(args);
		EntityCharacteristics ec=(EntityCharacteristics) args[0];
		Assert.assertNotNull(ec);
		this.ec=ec;
		//this.myBackPack=new HashMap<Observation, Integer>();
		//this.myBackPack.put(Observation.GOLD, 0);
		//this.myBackPack.put(Observation.DIAMOND, 0);
		this.aoe=new AgentObservableElement(ec.getAgentName());

		this.environmentName="env";
		//TODO move the environment name in the configuration file

		//get the parameters given into the object[]. In the current case, the environment where the agent will evolve	
		if(args[1]!=null){
			gateKeeperName=(String)args[1];	
		}else{
			Debug.error("Malfunction during parameter's loading of agent"+ this.getClass().getName());
		}

		PdeployMe p= new PdeployMe();
		addBehaviour(p.new R1deployMe(gateKeeperName,this));
		addBehaviour(p.new R1managerAnswer(gateKeeperName,this));

		if (ec.getMyEntityType()==EntityType.AGENT_TANKER){
			//addBehaviour(new ReceiveTreasureTankerBehaviour(this,myBackPack));
			addBehaviour(new ReceiveTreasureTankerBehaviour(this,this.aoe,this.ec));
		}

		//This protocol should be added in deltas with abilities as its more general than Dedale
		Pterminate p1= new Pterminate();
		addBehaviour(p1.new R2receiveTerminate(this));

		System.out.println("Agent "+this.getLocalName()+ " is started\n");
	}



	/*
	 * *****************
	 * Agent Destruction
	 * *****************
	 */

	protected void takeDown(){
		Debug.warning(getLocalName()+": I'm diying, that's life; \"Blow, wind! Come, wrack! At least I’ll die with harness on my back.\" (MB)");
		this.realEnv.removeEntity(this.getLocalName(),this.ec);// already done if the agent dies consequently to its move (well) but should be called is its the user decision to kill its agent.
		super.takeDown();

	}

	/*
	 * ********************
	 * MOBILITY COMPONENTS
	 * ********************
	 */


	protected void beforeMove() {
		super.beforeMove();
		try {
			System.out.println(this.getLocalName()+" : I'm leaving "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			e.printStackTrace();
		}
		//System.out.println("I quit");
	}

	@Override
	protected void afterMove() {
		super.afterMove();
		ContainerController c=this.getContainerController();

		try {
			System.out.println(this.getLocalName()+ ": I'm arrived at "+c.getContainerName());
		} catch (ControllerException e) {
			e.printStackTrace();
		}

		PdeployMe p= new PdeployMe();	
		this.addBehaviour(p.new R1deployMe(gateKeeperName,this));
	}





	// see DELTA
	//	public void addAbility(Ability ability,String abilityID,String role,List<List<Object>> behavioursParameters,Knowledge knowledge){
	//		//Call to the protocol class that will get me all the behaviours necessary to add to the agent regarding the role r
	//
	//		//specific knowledge component required for the protocol
	//		String conversationID=this.setKnowledge(abilityID, knowledge);
	//
	//		//the conversationID must be given to the behaviours. Even if they may not use it
	//		for (List<Object> lo:behavioursParameters){
	//			lo.add(conversationID);
	//		}
	//		
	//		List<Behaviour> lb=ability.getBehaviours(role,behavioursParameters);
	//		for (Behaviour b:lb){
	//			this.addBehaviour(b);
	//		}
	//	}

}

