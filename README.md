# Welcome to the Dedale project.

Dedale aims to facilitate and improve the experimental evaluation conditions of MAS algorithms and to contribute to the progress of the field towards decentralised solutions able to deal with real-world situations. Dedale is dedicated to the study of multi-agents coordination, learning and decision-making problems under real-life hypotheses. 

Dedale offers open, dynamic, asynchronous and partially observable environments. It allows to tackle either cooperative or competitive exploration, patrolling, pickup and delivery, treasure(s) or agent(s) hunt problems with teams from one to dozens of heterogeneous agents in discrete or continuous environments. These strengths make Dedale able to become a unifying environment for both MAS research and teaching communities in their goal to work and evaluate their proposals under realistic hypotheses.

This project is developed at Sorbonne University, Paris, France. It is used in both research and teaching activities. Considering the later, during the FoSyMa course (from the French “Fondement des Systèmes Multi-Agents”) as a practical application of Multi-Agents Systems (MAS). 
It allows Master's students to obtain a first-hand experience of some of the nice (and sometimes difficult) characteristics that comes with agents and distributed systems :
 - distribution and asynchronism (system and communication),
 - autonomy, decision and coordination in uncertain and partially observable environments

The Dedale project is currently composed of two repositories :
 - dedale, this repository, is used by the team and researchers to add new features, test, and configure specific variations
 - [dedale-etu](https://gitlab.com/dedale/dedale-etu) is the intanciation of the Dedale environment for the end-user. It offers several maps and options regarding the types of agents and the situations they will encounter on the maps. See [Dedale's presentation](https://dedale.gitlab.io/page/dedale/presentation/) for more details.

## Dedale's website : https://dedale.gitlab.io/
